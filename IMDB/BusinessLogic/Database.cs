﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Configuration;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.Globalization;
using Telerik.Web.UI;

namespace BusinessLogic
{
    public class Database
    {
        //  DLBase dbBase = new DLBase();
        SqlCommand cmd;



        public SqlConnection cxn;
        public SqlTransaction tran;



        #region Connect

        public SqlConnection GetConnection()
        {
            SqlConnection lCon;
            //lCon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            lCon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            lCon.Open();
            return lCon;
        }
        #endregion

        #region DisConnect

        public void DisposeConnection(SqlConnection lCon)
        {
            lCon.Close();
            lCon.Dispose();
        }
        #endregion



        #region Insert/Update/Delete Operation

        public Boolean InsertUpdateDeleteRow(string strProc, object objClsObject, ParameterCollection parCollection = null)
        {
            bool result = false;

            SqlConnection lCon = GetConnection();
            try
            {
                //Connect();

                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            if (p.PropertyType.Name.ToLower() == "datatable")
                            {
                                cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null));
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                            }
                            
                        }
                    }
                }
                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }
                if (cmd.ExecuteNonQuery() > 0)
                    result = true;
                else
                    result = false;
                DisposeConnection(lCon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
            return result;

        }
        #endregion


        #region Execute Non Query with return value

        public string ExecNonQueryWithReturnValue(string strProc, SqlParameter[] parCollection = null)
        {

            SqlConnection lCon = GetConnection();
            try
            {
                // Connect();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;

                if (parCollection != null)
                {
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                    }
                }

                return cmd.ExecuteNonQuery().ToString();
                DisposeConnection(lCon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }
        #endregion


        # region ReturnScalorWithSql

        public string ReturnScalerSQL(string strProc, SqlParameter[] parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = GetConnection();
            
            try
            {
                //Connect();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;

                if (parCollection != null)
                {
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                    }
                }
                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                        }
                    }
                }

                return Convert.ToString(cmd.ExecuteScalar());
                DisposeConnection(lCon);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }

        }

        #endregion


        #region Execute Non Query

        public Boolean ExecNonQuery(string strProc, SqlParameter[] parCollection = null)
        {

            SqlConnection lCon = GetConnection();
            bool result = false;
            try
            {
                //Connect();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;

                if (parCollection != null)
                {
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                    }
                }

                if (cmd.ExecuteNonQuery() > 0)
                    result = true;
                else
                    result = false;
                DisposeConnection(lCon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
            return result;
        }
        #endregion

        #region Execute Non Query With Out Parameter

        public SqlParameter[] ExecNonQueryWithOutParameter(string strProc, SqlParameter[] parCollection = null)
        {
            bool result = false;

            SqlConnection lCon = GetConnection();
            SqlParameter[] sqlPara = new SqlParameter[2];
            try
            {
                //Connect();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;

                if (parCollection != null)
                {
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                        cmd.Parameters[i].Direction = parCollection[i].Direction;
                        cmd.Parameters[i].SqlDbType = parCollection[i].SqlDbType;
                        cmd.Parameters[i].Size = parCollection[i].Size;
                    }
                }

                if (cmd.ExecuteNonQuery() > 0)
                    result = true;
                else
                    result = false;

                sqlPara[0] = new SqlParameter("result", result);    //set ExecuteNonQuery result                         
                sqlPara[1] = new SqlParameter("outParam1", cmd.Parameters["@outParam1"].Value); //set out parameter value
                DisposeConnection(lCon);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
            return sqlPara;
        }
        #endregion

        #region FillingDataGridView

       
        public void FillGridView(string strProc, RadGrid objDg, ParameterCollection parCollection = null)
        {
            //   DLBase dl = new DLBase();
            SqlConnection lCon = GetConnection(); //dl.GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            DataView objDv = null;
            try
            {
                //Connect();
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.Connection = lCon;
                cmd.CommandTimeout = 0;
                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;
                objDa.Fill(objDt);
                objDv = new DataView(objDt);
                objDg.DataSource = objDv;
                // objDg.DataBind();
                DisposeConnection(lCon); //dl.DisposeConnection(lCon);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDv = null;
                objDa = null;
                objDg = null;
                if (lCon != null)
                {
                    DisposeConnection(lCon); //dl.DisposeConnection(lCon);
                }
            }
        }


        public void FillRepeater(string strProc, Repeater objDg, ParameterCollection parCollection = null)
        {
            //   DLBase dl = new DLBase();
            SqlConnection lCon = GetConnection(); //dl.GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            DataView objDv = null;
            try
            {
                //Connect();
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.Connection = lCon;
                cmd.CommandTimeout = 0;
                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;
                objDa.Fill(objDt);
                objDv = new DataView(objDt);
                objDg.DataSource = objDv;
                objDg.DataBind();
                DisposeConnection(lCon); //dl.DisposeConnection(lCon);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDv = null;
                objDa = null;
                objDg = null;
                if (lCon != null)
                {
                    DisposeConnection(lCon); //dl.DisposeConnection(lCon);
                }
            }
        }
        #endregion

        #region FillingDropDown


        public void FillingDropDown(string strProc, RadComboBox objDrp, ParameterCollection parCollection = null, string initialmsg = "--Please Select--")
        {
            //   DLBase dl = new DLBase();
            SqlConnection lCon = GetConnection(); //dl.GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            try
            {
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;

                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;

                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);
                if (initialmsg != "") // updated for no intial message
                {
                    objDt.Rows.InsertAt(objDt.NewRow(), 0); //add new row in dataset
                    objDt.Rows[0][1] = "---Please Select---";
                    objDt.Rows[0][0] = "0";
                }
                //objDt.Rows.InsertAt(objDt.NewRow(), 0); //add new row in dataset
                //objDt.Rows[0][1] = "---Please Select---";
                //objDt.Rows[0][0] = "0";

                objDrp.DataSource = objDt;
                objDrp.DataTextField = objDt.Columns[1].ColumnName;
                objDrp.DataValueField = objDt.Columns[0].ColumnName;
                objDrp.DataBind();
                objDrp.SelectedIndex = 0;

                //objDrp.Items.Insert(0, new RadComboBoxItem("---Please Select---", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDrp = null;
                objDa = null;
                //DisConnect();
                if (lCon != null)
                {
                    DisposeConnection(lCon); //dl.DisposeConnection(lCon);
                }
            }

        }

        public void FillingDropDown(string strProc, DropDownList objDrp, ParameterCollection parCollection = null, string initialmsg = "--Please Select--")
        {
            //   DLBase dl = new DLBase();
            SqlConnection lCon = GetConnection(); //dl.GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            try
            {
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;

                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;

                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);

                if (initialmsg != "") // updated for no intial message
                {
                    objDt.Rows.InsertAt(objDt.NewRow(), 0); //add new row in dataset
                    objDt.Rows[0][1] = initialmsg;
                    objDt.Rows[0][0] = "0";                 
                }
                objDrp.DataSource = objDt;
                objDrp.DataTextField = objDt.Columns[1].ColumnName;
                objDrp.DataValueField = objDt.Columns[0].ColumnName;
                objDrp.DataBind();
                objDrp.SelectedIndex = 0;

                //objDrp.Items.Insert(0, new RadComboBoxItem("---Please Select---", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDrp = null;
                objDa = null;
                //DisConnect();
                if (lCon != null)
                {
                    DisposeConnection(lCon); //dl.DisposeConnection(lCon);
                }
            }

        }
        public void FillingRadioButtonList(string strProc, RadioButtonList objDrp, ParameterCollection parCollection = null, string initialmsg=null)
        {
            //   DLBase dl = new DLBase();
            SqlConnection lCon = GetConnection(); //dl.GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            try
            {
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;

                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;

                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);

                //if (initialmsg != "") // updated for no intial message
                //{
                //    objDt.Rows.InsertAt(objDt.NewRow(), 0); //add new row in dataset
                //    objDt.Rows[0][1] = initialmsg;
                //    objDt.Rows[0][0] = "0";
                //}
                objDrp.DataSource = objDt;
                objDrp.DataTextField = objDt.Columns[1].ColumnName;
                objDrp.DataValueField = objDt.Columns[0].ColumnName;
                objDrp.DataBind();
                objDrp.SelectedIndex = 0;

                //objDrp.Items.Insert(0, new RadComboBoxItem("---Please Select---", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDrp = null;
                objDa = null;
                //DisConnect();
                if (lCon != null)
                {
                    DisposeConnection(lCon); //dl.DisposeConnection(lCon);
                }
            }

        }


        public void FillingTelerikDropDown(string strProc, RadComboBox objDrp, ParameterCollection parCollection = null, string initialmsg = "--Please Select--")
        {
            //   DLBase dl = new DLBase();
            SqlConnection lCon = GetConnection(); //dl.GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            try
            {
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;

                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;

                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);

                if (initialmsg != "") // updated for no intial message
                {
                    objDt.Rows.InsertAt(objDt.NewRow(), 0); //add new row in dataset
                    objDt.Rows[0][1] = "---Please Select---";
                    objDt.Rows[0][0] = "0";
                }
                objDrp.DataSource = objDt;
                objDrp.DataTextField = objDt.Columns[1].ColumnName;
                objDrp.DataValueField = objDt.Columns[0].ColumnName;
                objDrp.DataBind();
                objDrp.SelectedIndex = 0;

                //objDrp.Items.Insert(0, new RadComboBoxItem("---Please Select---", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDrp = null;
                objDa = null;
                //DisConnect();
                if (lCon != null)
                {
                    DisposeConnection(lCon); //dl.DisposeConnection(lCon);
                }
            }

        }


        //public Dictionary<int, string> FillingDropDown(string strProc, ParameterCollection parCollection = null)
        //{
        //    Dictionary<int, string> dic = new Dictionary<int, string>();
        //    try
        //    {

        //        using (SqlConnection sqlConn = GetConnection())
        //        {

        //            using (SqlCommand cmd = new SqlCommand(strProc, sqlConn))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.CommandTimeout = 0;
        //                if (parCollection != null)
        //                {
        //                    foreach (Parameter par in parCollection)
        //                    {
        //                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
        //                    }
        //                }
        //                using (SqlDataReader reader = cmd.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {
        //                        dic.Add(Convert.ToInt32(reader[0]), Convert.ToString(reader[1]));
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //    return dic;
        //}

        public void FillingListBox(string strProc, RadListBox objDrp, ParameterCollection parCollection = null)
        {
            //SqlConnection lCon = GetConnection(); 
            SqlConnection lCon = GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            try
            {
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;


                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;

                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);

                //objDt.Rows.InsertAt(objDt.NewRow(), 0); //add new row in dataset
                //objDt.Rows[0][1] = "---Please Select---";
                //objDt.Rows[0][0] = "0";

                objDrp.DataSource = objDt;
                objDrp.DataTextField = objDt.Columns[1].ColumnName;
                objDrp.DataValueField = objDt.Columns[0].ColumnName;
                objDrp.DataBind();
                objDrp.SelectedIndex = 0;

                //objDrp.Items.Insert(0, new RadComboBoxItem("---Please Select---", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objDrp = null;
                objDa = null;
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }

        }


        #endregion

        #region Return Scaler



        public string ReturnScaler(string strProc, ParameterCollection parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                //Connect();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;

                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                        }
                    }
                }

                return Convert.ToString(cmd.ExecuteScalar());
                DisposeConnection(lCon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        #endregion

        #region Return Dateset

        public DataSet ReturnDataSet(string strProc, ParameterCollection parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                DataSet objDs = new DataSet();
                objDa.SelectCommand = cmd;

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //if (par.DbType == DbType.Int32)
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, Convert.ToInt32(par.DefaultValue));
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //}
                    }
                }

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                        }
                    }
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDs);
                return objDs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        public DataSet ReturnDataSetWithTimeout(string strProc, ParameterCollection parCollection = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;
                DataSet objDs = new DataSet();
                objDa.SelectCommand = cmd;

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //if (par.DbType == DbType.Int32)
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, Convert.ToInt32(par.DefaultValue));
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //}
                    }
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDs);
                return objDs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }



        public SqlDataReader ReturnDataReader(string strProc, ParameterCollection parCollection = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                //  DataSet objDs = new DataSet();
                objDa.SelectCommand = cmd;

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //if (par.DbType == DbType.Int32)
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, Convert.ToInt32(par.DefaultValue));
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //}
                    }
                }
                //Connect();
                cmd.Connection = lCon;
                // objDa.Fill(objDs);
                SqlDataReader dr = cmd.ExecuteReader();

                return cmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        #endregion

        #region Return DataTable

        public DataTable ReturnDataTable(string strProc, ParameterCollection parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;
                //DataSet objDs = new DataSet();
                DataTable objDt = new DataTable();
                objDa.SelectCommand = cmd;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                        }
                    }
                }
                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //if (par.DbType == DbType.Int32)
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, Convert.ToInt32(par.DefaultValue));
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //}
                    }
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);
                DisposeConnection(lCon);
                return objDt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        public DataTable ReturnDataTableSQLObject(string strProc, SqlParameter[] sparCollection = null,ParameterCollection parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;
                //DataSet objDs = new DataSet();
                DataTable objDt = new DataTable();
                objDa.SelectCommand = cmd;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, DBNull.Value));
                            //cmd.sqlParameter.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            //cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, p.GetValue(objClsObject, null)));
                        }
                    }
                }
                if (sparCollection != null)
                {
                    //foreach (Parameter par in parCollection)
                    //{
                    //cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    for (int i = 0; i < sparCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(sparCollection[i].ParameterName, sparCollection[i].Value);
                    }
                    //}
                }

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);
                DisposeConnection(lCon);
                return objDt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        public DataSet ReturnDataSetSQLObject(string strProc, SqlParameter[] sparCollection = null, ParameterCollection parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                DataSet objDs = new DataSet();
                objDa.SelectCommand = cmd;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, DBNull.Value));
                            //cmd.sqlParameter.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            //cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, p.GetValue(objClsObject, null)));
                        }
                    }
                }

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //if (par.DbType == DbType.Int32)
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, Convert.ToInt32(par.DefaultValue));
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //}
                    }
                }

                if (sparCollection != null)
                {
                    //foreach (Parameter par in parCollection)
                    //{
                    //cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    for (int i = 0; i < sparCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(sparCollection[i].ParameterName, sparCollection[i].Value);
                    }
                    //}
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDs);
                return objDs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        public DataSet ReturnDataSetSQLObjectTimeout(string strProc, SqlParameter[] sparCollection = null, ParameterCollection parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;
                DataSet objDs = new DataSet();
                objDa.SelectCommand = cmd;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.Add( new SqlParameter("@" + p.Name, DBNull.Value));
                            //cmd.sqlParameter.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            //cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, p.GetValue(objClsObject, null)));
                        }
                    }
                }

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //if (par.DbType == DbType.Int32)
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, Convert.ToInt32(par.DefaultValue));
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                        //}
                    }
                }

                if (sparCollection != null)
                {
                    //foreach (Parameter par in parCollection)
                    //{
                    //cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    for (int i = 0; i < sparCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(sparCollection[i].ParameterName, sparCollection[i].Value);
                    }
                    //}
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDs);
                return objDs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        public DataTable ReturnDataTableSQL(string strProc, SqlParameter[] parCollection = null, object objClsObject = null)
        {

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                SqlDataAdapter objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;
                //DataSet objDs = new DataSet();
                DataTable objDt = new DataTable();
                objDa.SelectCommand = cmd;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, DBNull.Value));
                            //cmd.sqlParameter.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            //cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null).ToString());
                            cmd.Parameters.Add(new SqlParameter("@" + p.Name, p.GetValue(objClsObject, null)));
                        }
                    }
                }
                if (parCollection != null)
                {
                    //foreach (Parameter par in parCollection)
                    //{
                    //cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                    }
                    //}
                }
                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);
                DisposeConnection(lCon);
                return objDt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
        }

        #endregion

        //#region FillingListBox


        //public void FillingListBox(string strProc, CheckBoxList objLst, ParameterCollection parCollection = null)
        //{
        //    SqlDataAdapter objDa = null;
        //    DataTable objDt = null;
        //    //   DLBase dl = new DLBase();
        //    SqlConnection lCon = null;
        //    try
        //    {
        //        lCon = GetConnection(); //dl.GetConnection();
        //        objDa = new SqlDataAdapter();
        //        cmd = new SqlCommand();
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = strProc;


        //        //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

        //        if (parCollection != null)
        //        {
        //            foreach (Parameter par in parCollection)
        //            {
        //                cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
        //            }
        //        }

        //        objDt = new DataTable();
        //        objDa.SelectCommand = cmd;

        //        //Connect();
        //        cmd.Connection = lCon;
        //        objDa.Fill(objDt);

        //        objLst.DataSource = objDt;
        //        objLst.DataTextField = objDt.Columns[1].ColumnName;
        //        objLst.DataValueField = objDt.Columns[0].ColumnName;
        //        objLst.DataBind();
        //        DisposeConnection(lCon); //dl.DisposeConnection(lCon);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //    finally
        //    {
        //        objDt = null;
        //        objLst = null;
        //        objDa = null;
        //        if (lCon != null)
        //        {
        //            DisposeConnection(lCon); //dl.DisposeConnection(lCon);
        //        }
        //    }

        //}
        //#endregion

        #region Insert/Update/Delete Operation with SQL parameter

        public Boolean InsertUpdateDeleteWithSQLParam(string strProc, object objClsObject, SqlParameter[] parCollection = null)
        {
            bool result = false;

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null));
                        }
                    }
                }
                if (parCollection != null)
                {
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                    }
                }
                if (cmd.ExecuteNonQuery() > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
            return result;

        }
        #endregion

        public Boolean InsertUpdateDeleteWithSQLParamForBillGeneration(string strProc, object objClsObject, SqlParameter[] parCollection = null)
        {
            bool result = false;

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;
                cmd.CommandTimeout = 0;
                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null));
                        }
                    }
                }
                if (parCollection != null)
                {
                    for (int i = 0; i < parCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(parCollection[i].ParameterName, parCollection[i].Value);
                    }
                }
                if (cmd.ExecuteNonQuery() > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
            return result;

        }

        #region Insert/Update/Delete Operation with SQL parameter

        public Boolean InsertUpdateDeleteWithSQLParam(string strProc, object objClsObject, ParameterCollection parCollection = null,SqlParameter[] sparCollection = null)
        {
            bool result = false;

            SqlConnection lCon = null;
            try
            {
                lCon = GetConnection();
                cmd = new SqlCommand();
                cmd.Connection = lCon;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;

                if (objClsObject != null)
                {
                    //loop through all properties of the class object and add parameters for each properties into procedure                 
                    foreach (System.Reflection.PropertyInfo p in objClsObject.GetType().GetProperties())
                    {
                        if (p.GetValue(objClsObject, null) == "pass null") //for allow null field
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, DBNull.Value);
                        }
                        else if (p.GetValue(objClsObject, null) != null)
                        {
                            cmd.Parameters.AddWithValue("@" + p.Name, p.GetValue(objClsObject, null));
                        }
                    }
                }
                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }
                if (sparCollection != null)
                {
                    for (int i = 0; i < sparCollection.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(sparCollection[i].ParameterName, sparCollection[i].Value);
                    }
                }
                if (cmd.ExecuteNonQuery() > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }
            return result;

        }
        #endregion

        public Boolean SetReport(ReportViewer rviewObj, string lstrReportFileName, ReportParameterCollection rparamCollection)
        {
            bool result = false;
            try
            {
                ConnectReport(rviewObj, lstrReportFileName);
                rviewObj.ServerReport.SetParameters(rparamCollection);
                rviewObj.ServerReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;

        }

        public Byte[] ExportReportToPDF(string lstrReportFileName, ReportParameterCollection rparamCollection)
        {
            Byte[] result;
            try
            {
                Microsoft.Reporting.WebForms.ReportViewer rviewObj = new Microsoft.Reporting.WebForms.ReportViewer();

                ConnectReport(rviewObj, lstrReportFileName);
                rviewObj.ServerReport.SetParameters(rparamCollection);

                string mimeType, encoding, extension;
                string[] streamids; Microsoft.Reporting.WebForms.Warning[] warnings;
                string format = "PDF";
                result = rviewObj.ServerReport.Render(format, "", out mimeType, out encoding, out extension, out streamids, out warnings);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;

        }

        
        #region ConnectReport
        ///<author>Ankit Patel</author>
        ///<date>05 Aug 11 </date>
        /// <summary>establish database connection by getting connection string from web.config file</summary> 
        protected void ConnectReport(ReportViewer rviewObj, string lstrReportFileName)
        {

            //<add key="ReportServerUserId" value="Administrator" />
            //<add key="ReportServerPassword" value="gipl123" />
            //<add key="ReportServerHost" value="192.168.45.243" />
            rviewObj.ServerReport.ReportServerUrl = new System.Uri(System.Configuration.ConfigurationManager.AppSettings["ReportServerIP"]);
            rviewObj.ServerReport.ReportPath = "/" + lstrReportFileName;
            //rviewObj.ServerReport.ReportServerCredentials = new ReportCredentials(System.Configuration.ConfigurationManager.AppSettings["ReportServerUserId"], System.Configuration.ConfigurationManager.AppSettings["ReportServerPassword"], System.Configuration.ConfigurationManager.AppSettings["ReportServerHost"]);
        }
        #endregion

        public void FillingTree(string strProc, RadTreeView objMenu, ParameterCollection parCollection = null)
        {
            //DLBase dl = new DLBase();

            SqlConnection lCon = GetConnection();
            SqlDataAdapter objDa = null;
            DataTable objDt = null;
            try
            {
                objDa = new SqlDataAdapter();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = strProc;


                //loop through all properties of the ParameterCollection and add parameters for each properties into procedure   

                if (parCollection != null)
                {
                    foreach (Parameter par in parCollection)
                    {
                        cmd.Parameters.AddWithValue(par.Name, par.DefaultValue);
                    }
                }

                objDt = new DataTable();
                objDa.SelectCommand = cmd;

                //Connect();
                cmd.Connection = lCon;
                objDa.Fill(objDt);
                objMenu.DataSource = objDt;
                objMenu.DataTextField = objDt.Columns[1].ColumnName;
                objMenu.DataValueField = objDt.Columns[0].ColumnName;
                objMenu.DataFieldParentID = objDt.Columns[2].ColumnName;
                objMenu.DataFieldID = objDt.Columns[0].ColumnName;
                //objMenu.DataNavigateUrlField = objDt.Columns[3].ColumnName;
                objMenu.DataBind();


                //objDrp.Items.Insert(0, new RadComboBoxItem("---Please Select---", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                objDt = null;
                objMenu = null;
                objDa = null;
                //DisConnect();
                if (lCon != null)
                {
                    DisposeConnection(lCon);
                }
            }

        }

        #region Buk Insert
        public Int32 SQLBulkCopy(DataTable DT1, String Tablename)
        {
            //SqlConnection lCon;
            try
            {
                //lCon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ImportConnectionString"]);
                //lCon.Open();

                using (SqlBulkCopy bcp = new SqlBulkCopy(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString(), SqlBulkCopyOptions.FireTriggers))
                {
                    bcp.DestinationTableName = Tablename;
                    bcp.BulkCopyTimeout = 1000;
                    bcp.WriteToServer(DT1);
                }

                return 1;
            }
            catch (Exception ex)
            {
                BusinessLogic.Error.WriteToLogFile("Error in SQLBulkCopy  : " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
                return 0;
            }
            //finally
            //{
            //      lCon.Close();
            //      lCon.Dispose();
            //}

        }
        #endregion

    }
}
