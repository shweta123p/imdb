﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace BusinessLogic
{
    public class Actor
    {
        public Boolean InsertActor(string actorname, string gender, string bdate, string bio)
        {
            Database objDLCommon = new Database();
            Boolean status = false;
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@name", DbType.String, actorname.ToString()));
                pm.Add(new Parameter("@gender", DbType.String, gender.ToString()));
                pm.Add(new Parameter("@dateofBirth", DbType.String, bdate.ToString()));
                pm.Add(new Parameter("@bio", DbType.String, bio.ToString()));

                status = objDLCommon.InsertUpdateDeleteRow("InsertActor", null, pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public Boolean UpdateActor(string actorId, string actorname, string gender, string bdate, string bio)
        {
            Database objDLCommon = new Database();
            Boolean status = false;
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@actorId", DbType.String, actorId.ToString()));
                pm.Add(new Parameter("@name", DbType.String, actorname.ToString()));
                pm.Add(new Parameter("@gender", DbType.String, gender.ToString()));
                pm.Add(new Parameter("@dateofBirth", DbType.String, bdate.ToString()));
                pm.Add(new Parameter("@bio", DbType.String, bio.ToString()));

                status = objDLCommon.InsertUpdateDeleteRow("UpdateActor", null, pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public void FillActor(DropDownList objDrp)
        {
            try
            {
                Database objDLCommon = new Database();
                objDLCommon.FillingDropDown("FillActor", objDrp, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ActorList()
        {
            DataTable dt = new DataTable();
            try
            {
                Database objDLCommon = new Database();
                dt = objDLCommon.ReturnDataTable("ActorList", null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable ActorListById(string actorId)
        {
            DataTable dt = new DataTable();
            try
            {
                Database objDLCommon = new Database();

                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@actorId", DbType.String, actorId.ToString()));

                dt = objDLCommon.ReturnDataTable("ActorListById", pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

    }
}
