﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace BusinessLogic.common
{
    public class Log
    {
        public DataTable InsertASPWSLog(string GSTIN, string strAction, string ActivityLogId, string responseData, 
                string referenceId, string status_cd, string errorMsg, string errorCode, string createdBy,string returnPeriod=null,string APIUploadId = null,string uploadStatus=null)
        {
            //Boolean status = false;
            DataTable dt = new DataTable();
            try
            {
                Database objDatabase = new Database();
                ParameterCollection parCollection = new ParameterCollection();
                parCollection.Add(new Parameter("@GSTIN", DbType.String, GSTIN));
                parCollection.Add(new Parameter("@Action", DbType.String, strAction));
                parCollection.Add(new Parameter("@ActivityLogId", DbType.String, ActivityLogId));
                parCollection.Add(new Parameter("@responseData", DbType.String, responseData));
                parCollection.Add(new Parameter("@referenceId", DbType.String, referenceId));
                parCollection.Add(new Parameter("@status_cd", DbType.String, status_cd));
                parCollection.Add(new Parameter("@errorMsg", DbType.String, errorMsg));
                parCollection.Add(new Parameter("@errorCode", DbType.String, errorCode));
                parCollection.Add(new Parameter("@createdBy", DbType.Int32, createdBy));
                parCollection.Add(new Parameter("@returnPeriod", DbType.String, returnPeriod));
                parCollection.Add(new Parameter("@APIUploadId", DbType.String, APIUploadId));
                parCollection.Add(new Parameter("@uploadStatus", DbType.String, uploadStatus));
                dt = objDatabase.ReturnDataTable("insertASPWSLog", parCollection);
                //status = objDatabase.InsertUpdateDeleteRow("insertASPWSLog", null, parCollection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return status;
            return dt;
        }
        public  DataTable GetGSTR1uploadStatus(string gstin,string returnPeriod,string action)
        {
            DataTable dt = new DataTable();
            ParameterCollection parCollection = new ParameterCollection();
            Database objDLCommon = new Database();
            parCollection.Add(new Parameter("@gstin", DbType.String, gstin));
            parCollection.Add(new Parameter("@returnPeriod", DbType.String, returnPeriod));
            parCollection.Add(new Parameter("@action", DbType.String, action));
            dt = objDLCommon.ReturnDataTable("GetGSTR1uploadStatus", parCollection);
            return dt;
        }

        public DataTable GetGSTR1uploadStatusById(string APIUploadId)
        {
            DataTable dt = new DataTable();
            ParameterCollection parCollection = new ParameterCollection();
            Database objDLCommon = new Database();
            parCollection.Add(new Parameter("@APIUploadId", DbType.String, APIUploadId));
            dt = objDLCommon.ReturnDataTable("GetGSTR1uploadStatusById", parCollection);
            return dt;
        }

        public Boolean InsertReturnFileSummary(string GSTIN, string ReturnPeriod, string GSTR1Submit, string GSTR1File, string GSTR2Submit, string GSTR2File, string GSTR3Submit, string GSTR3File, string CreatedBy)
        {
            Database objDLCommon = new Database();
            //DataTable dt = new DataTable();
            Boolean status = false;
            try
            {
                ParameterCollection parCollection = new ParameterCollection();
                parCollection.Add(new Parameter("@GSTIN", DbType.String, GSTIN));
                parCollection.Add(new Parameter("@ReturnPeriod", DbType.String, ReturnPeriod));
                parCollection.Add(new Parameter("@GSTR1Submit", DbType.String, GSTR1Submit));
                parCollection.Add(new Parameter("@GSTR1File", DbType.String, GSTR1File));
                parCollection.Add(new Parameter("@GSTR2Submit", DbType.String, GSTR2Submit));
                parCollection.Add(new Parameter("@GSTR2File", DbType.String, GSTR2File));
                parCollection.Add(new Parameter("@GSTR3Submit", DbType.String, GSTR3Submit));
                parCollection.Add(new Parameter("@GSTR3File", DbType.String, GSTR3File));
                parCollection.Add(new Parameter("@CreatedBy", DbType.String, CreatedBy));
             

                status = objDLCommon.InsertUpdateDeleteRow("InsertReturnFileSummary",null, parCollection);
                return status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public DataTable UpdateReturnStatusForGSTR3BSubmit(string GSTIN, string ReturnPeriod, string modifiedBy)
        //{
        //    DataTable dt = new DataTable();
        //    ParameterCollection parCollection = new ParameterCollection();
        //    Database objDLCommon = new Database();
        //    parCollection.Add(new Parameter("@GSTIN", DbType.String, GSTIN));
        //    parCollection.Add(new Parameter("@ReturnPeriod", DbType.String, ReturnPeriod));
        //    parCollection.Add(new Parameter("@ModifiedBy", DbType.String, modifiedBy));
            
        //    dt = objDLCommon.ReturnDataTable("UpdateReturnStatusForGSTR3BSubmit", parCollection);
        //    return dt;
        //}

        //public DataTable UpdateReturnStatusForGSTR3BFile(string GSTIN, string ReturnPeriod,string modifiedBy)
        //{
        //    DataTable dt = new DataTable();
        //    ParameterCollection parCollection = new ParameterCollection();
        //    Database objDLCommon = new Database();
        //    parCollection.Add(new Parameter("@GSTIN", DbType.String, GSTIN));
        //    parCollection.Add(new Parameter("@ReturnPeriod", DbType.String, ReturnPeriod));
        //    parCollection.Add(new Parameter("@ModifiedBy", DbType.String, modifiedBy));

        //    dt = objDLCommon.ReturnDataTable("UpdateReturnStatusForGSTR3BFile", parCollection);

        //    return dt;
        //}

        public DataTable GetGSTR4uploadStatus(string gstin, string returnPeriod, string action)
        {
            DataTable dt = new DataTable();
            ParameterCollection parCollection = new ParameterCollection();
            Database objDLCommon = new Database();
            parCollection.Add(new Parameter("@gstin", DbType.String, gstin));
            parCollection.Add(new Parameter("@returnPeriod", DbType.String, returnPeriod));
            parCollection.Add(new Parameter("@action", DbType.String, action));
            dt = objDLCommon.ReturnDataTable("GetGSTR4uploadStatus", parCollection);
            return dt;
        }

        public DataTable GetGSTR4uploadStatusById(string APIUploadId)
        {
            DataTable dt = new DataTable();
            ParameterCollection parCollection = new ParameterCollection();
            Database objDLCommon = new Database();
            parCollection.Add(new Parameter("@APIUploadId", DbType.String, APIUploadId));
            dt = objDLCommon.ReturnDataTable("GetGSTR4uploadStatusById", parCollection);
            return dt;
        }


    }
}