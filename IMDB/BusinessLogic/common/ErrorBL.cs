﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace BusinessLogic.common
{
    public class ErrorBL
    {
        //public int Id { get; set; }
        public string Message { get; set; }
        public string IpAddress { get; set; }
        public string Source { get; set; }
        public string UserName { get; set; }
        public string Browser { get; set; }
        public string ErrorDate { get; set; }

        
        public Boolean InsertError(ErrorBL objError)
        {
            Database objDLCommon = new Database();
            Boolean lbstatus = false;

            try
            {
                lbstatus = objDLCommon.InsertUpdateDeleteRow("InsertError", objError);
                return lbstatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
