﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Configuration;
using System.Security.Cryptography;

namespace BusinessLogic
{
    public class Validation
    {

        public static string ChangeFileName(string filename)
        {
            string newFileName;
            try
            {
                string[] Substring;
                char[] delimitdot = { '.' };
                Substring = filename.Replace(" ", "").Split(delimitdot, 2);
                newFileName = RemoveSpecialCharacter(Substring[0]) + DateTime.Now.ToString("MMddyyyyhhmmssfff") + '.' + Substring[1];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return newFileName;
        }


        #region SQLInjectionPrevention
        public static Boolean PreventSQLInjection(String lStrInputValue, Boolean flag)
        {
            Boolean lbooStatus = true;
            if (lStrInputValue.Equals(""))
            {
                //  lbooStatus = true;
                return lbooStatus;
            }
            else
            {
                for (int lIntSQL = 0; lIntSQL < lStrInputValue.Length; lIntSQL++)
                {
                    char ch = lStrInputValue[lIntSQL];
                    if (ch == ',' || ch == '\'' || ch == '\"')
                    {
                        lbooStatus = false;
                    }

                }
            }
            if (flag)
            {
                lbooStatus = !lbooStatus;
            }
            return lbooStatus;

        }
        #endregion

        // Writing Log File - Ravi

        public static String ConvertAmounttoText(Double ldTextAmount)
        {

            String lStrReturnText = "";
            String lStrRupee = "";
            String lStrPaisa = "";
            Int64 lIntRupee = Convert.ToInt64(Math.Truncate(ldTextAmount));
            // MessageBox.Show(Convert.ToString(lIntRupee));
            Int64 lIntPaisa = Convert.ToInt64((ldTextAmount - lIntRupee) * 100);
            // MessageBox.Show(Convert.ToString(lIntPaisa));
            lStrRupee = ConvertAmountRupee(lIntRupee);
            lStrPaisa = ConvertAmountRupee(lIntPaisa);
            //MessageBox.Show("Return Rupees " + lStrReturnText);


            if (!(lStrRupee.Equals("")))
            {
                lStrReturnText = " Rupees " + lStrRupee;
            }
            if (!(lStrPaisa.Equals("")))
            {
                lStrReturnText = lStrReturnText + lStrPaisa + " Paisa";
            }
            if (!(lStrReturnText.Equals("")))
            {
                lStrReturnText = lStrReturnText + " Only";
            }

            return lStrReturnText;
        }
        public static String ConvertAmountRupee(Int64 lIntTextAmount)
        {

            String lStrReturnText = "";
            //  MessageBox.Show("Amount Received" + lIntTextAmount);
            Double lIntConvertAmount = lIntTextAmount;
            Int64 lIntArab = 0;
            Int64 lIntCrore = 0;
            Int64 lIntLac = 0;
            Int64 lIntThousand = 0;
            Int64 lIntHundred = 0;
            Int64 lIntRupee = 0;

            String lStrArab = "";
            String lStrCrore = "";
            String lStrLac = "";
            String lStrThousand = "";
            String lStrHundred = "";
            String lStrRupee = "";



            try
            {

                //    MessageBox.Show("NO Used : "+lIntConvertAmount.ToString("N0"));

                //  lIntArab =  Convert.ToInt32(lIntConvertAmount / 1000000000);
                lIntArab = Convert.ToInt64(Math.Floor(lIntConvertAmount / 1000000000));
                //   MessageBox.Show("ARab :" + lIntArab);
                if (lIntArab > 0)
                {
                    lIntConvertAmount = lIntConvertAmount - (lIntArab * Convert.ToInt64(1000000000));
                    // MessageBox.Show("Converted Amount : " + lIntConvertAmount);
                }


                lIntCrore = Convert.ToInt64(Math.Floor(lIntConvertAmount / 10000000));
                // lIntCrore = Convert.ToInt32(lIntConvertAmount / 10000000);
                if (lIntCrore > 0)
                {
                    lIntConvertAmount = lIntConvertAmount - (lIntCrore * Convert.ToInt64(10000000));
                }

                //  lIntLac = Convert.ToInt32(lIntConvertAmount / 100000);
                lIntLac = Convert.ToInt64(Math.Floor(lIntConvertAmount / 100000));
                if (lIntLac > 0)
                {
                    lIntConvertAmount = lIntConvertAmount - (lIntLac * Convert.ToInt64(100000));
                }

                //  lIntThousand = Convert.ToInt32(lIntConvertAmount / 1000);

                lIntThousand = Convert.ToInt64(Math.Floor(lIntConvertAmount / 1000));
                if (lIntThousand > 0)
                {
                    lIntConvertAmount = lIntConvertAmount - (lIntThousand * Convert.ToInt64(1000));
                }

                // lIntHundred = Convert.ToInt32(lIntConvertAmount / 100);

                lIntHundred = Convert.ToInt64(Math.Floor(lIntConvertAmount / 100));
                if (lIntHundred > 0)
                {
                    lIntConvertAmount = lIntConvertAmount - (lIntHundred * Convert.ToInt64(100));
                }
                //   MessageBox.Show("Before Rupee : "+Convert.ToString(lIntConvertAmount));
                lIntRupee = Convert.ToInt64(lIntConvertAmount / 1);

                //    MessageBox.Show("Lac : "+Convert.ToString(lIntLac));
                //     MessageBox.Show("Thousand : " + Convert.ToString(lIntThousand));
                //    MessageBox.Show("Hundred : " + Convert.ToString(lIntHundred));

                if (lIntArab > 9 && lIntArab < 21)
                {
                    lStrArab = tens(lIntArab);
                }
                else if (lIntArab > 20)
                {
                    Int64 lIntArabTemp = lIntArab / 10;
                    lIntArabTemp = lIntArabTemp * 10;
                    lStrArab = tens(lIntArabTemp);
                    if ((lIntArab - lIntArabTemp) > 0)
                    {
                        lStrArab = lStrArab + " " + ones(lIntArab - lIntArabTemp);
                    }
                }
                else if (lIntArab < 10 && lIntArab > 0)
                {
                    lStrArab = ones(lIntArab);
                }
                //   MessageBox.Show("Arab : " + lStrArab);


                if (lIntCrore > 9 && lIntCrore < 21)
                {
                    lStrCrore = tens(lIntCrore);
                }
                else if (lIntCrore > 20)
                {
                    Int64 lIntCroreTemp = lIntCrore / 10;
                    lIntCroreTemp = lIntCroreTemp * 10;
                    lStrCrore = tens(lIntCroreTemp);
                    if ((lIntCrore - lIntCroreTemp) > 0)
                    {
                        lStrCrore = lStrCrore + " " + ones(lIntCrore - lIntCroreTemp);
                    }
                }
                else if (lIntCrore < 10 && lIntCrore > 0)
                {
                    lStrCrore = ones(lIntCrore);
                }
                //    MessageBox.Show("Crore : " + lStrCrore);

                if (lIntLac > 9 && lIntLac < 21)
                {
                    lStrLac = tens(lIntLac);
                }
                else if (lIntLac > 20)
                {
                    Int64 lIntLacTemp = lIntLac / 10;
                    lIntLacTemp = lIntLacTemp * 10;
                    lStrLac = tens(lIntLacTemp);
                    if ((lIntLac - lIntLacTemp) > 0)
                    {
                        lStrLac = lStrLac + " " + ones(lIntLac - lIntLacTemp);
                    }
                }
                else if (lIntLac < 10 && lIntLac > 0)
                {
                    lStrLac = ones(lIntLac);
                }
                //    MessageBox.Show("Lac : " + lStrLac);


                if (lIntThousand > 9 && lIntThousand < 21)
                {
                    lStrThousand = tens(lIntThousand);
                }
                else if (lIntThousand > 20)
                {
                    Int64 lIntThousandTemp = lIntThousand / 10;
                    lIntThousandTemp = lIntThousandTemp * 10;
                    lStrThousand = tens(lIntThousandTemp);
                    if ((lIntThousand - lIntThousandTemp) > 0)
                    {
                        lStrThousand = lStrThousand + " " + ones(lIntThousand - lIntThousandTemp);
                    }
                }
                else if (lIntThousand < 10 && lIntThousand > 0)
                {
                    lStrThousand = ones(lIntThousand);
                }
                //    MessageBox.Show("Thousand : " + lStrThousand);


                if (lIntHundred > 9 && lIntHundred < 21)
                {
                    lStrHundred = tens(lIntHundred);
                }
                else if (lIntHundred > 20)
                {
                    Int64 lIntHundredTemp = lIntHundred / 10;
                    lIntHundredTemp = lIntHundredTemp * 10;
                    lStrHundred = tens(lIntHundredTemp);
                    if ((lIntHundred - lIntHundredTemp) > 0)
                    {
                        lStrHundred = lStrHundred + " " + ones(lIntHundred - lIntHundredTemp);
                    }
                }
                else if (lIntHundred < 10 && lIntHundred > 0)
                {
                    lStrHundred = ones(lIntHundred);
                }
                //     MessageBox.Show("Hundred : " + lStrHundred);


                if (lIntRupee > 9 && lIntRupee < 21)
                {
                    lStrRupee = tens(lIntRupee);
                }
                else if (lIntRupee > 20)
                {
                    Int64 lIntRupeeTemp = lIntRupee / 10;
                    lIntRupeeTemp = lIntRupeeTemp * 10;
                    lStrRupee = tens(lIntRupeeTemp);
                    if ((lIntRupee - lIntRupeeTemp) > 0)
                    {
                        lStrRupee = lStrRupee + " " + ones(lIntRupee - lIntRupeeTemp);
                    }
                }
                else if (lIntRupee < 10 && lIntRupee > 0)
                {
                    lStrRupee = ones(lIntRupee);
                }
                //     MessageBox.Show("Rupee : " + lStrRupee);

                if (lStrArab != "")
                {
                    lStrReturnText = lStrArab + " Arab ";
                }
                if (lStrCrore != "")
                {
                    lStrReturnText = lStrReturnText + lStrCrore + " Crore ";
                }
                if (lStrLac != "")
                {
                    lStrReturnText = lStrReturnText + lStrLac + " Lac ";
                }
                if (lStrThousand != "")
                {
                    lStrReturnText = lStrReturnText + lStrThousand + " Thousand ";
                }
                if (lStrHundred != "")
                {
                    lStrReturnText = lStrReturnText + lStrHundred + " Hundred ";
                }
                if (lStrRupee != "")
                {
                    lStrReturnText = lStrReturnText + lStrRupee;
                }

            }
            catch (Exception e)
            {
               // MessageBox.Show(e.Message);
                throw e;
            }
            return lStrReturnText;

        }

        private static String tens(Int64 digit)
        {

            String name = null;

            switch (digit)
            {

                case 10: name = "Ten";
                    break;

                case 11: name = "Eleven";
                    break;

                case 12: name = "Twelve";
                    break;

                case 13: name = "Thirteen";
                    break;

                case 14: name = "Fourteen";
                    break;
                case 15: name = "Fifteen";
                    break;
                case 16: name = "Sixteen";
                    break;
                case 17: name = "Seventeen";
                    break;
                case 18: name = "Eighteen";
                    break;
                case 19: name = "Nineteen";
                    break;
                case 20: name = "Twenty";
                    break;
                case 30: name = "Thirty";
                    break;
                case 40: name = "Forty";
                    break;
                case 50: name = "Fifty";
                    break;
                case 60: name = "Sixty";
                    break;
                case 70: name = "Seventy";
                    break;
                case 80: name = "Eighty";
                    break;
                case 90: name = "Ninety";
                    break;
                //default: if (digt > 0)
                //    {

                //          name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));

                //    }
                //    break;
            }

            return name;

        }

        private static String ones(Int64 digit)
        {

            String name = "";

            switch (digit)
            {

                case 1: name = "One";
                    break;

                case 2: name = "Two";
                    break;

                case 3: name = "Three";
                    break;

                case 4: name = "Four";
                    break;

                case 5: name = "Five";
                    break;

                case 6: name = "Six";
                    break;

                case 7: name = "Seven";
                    break;

                case 8: name = "Eight";
                    break;

                case 9: name = "Nine";
                    break;

            }

            return name;

        }

        public static string RemoveSpecialCharacter(string strToReplace)
        {
            strToReplace = strToReplace.Replace("~", "");
            strToReplace = strToReplace.Replace("`", "");
            strToReplace = strToReplace.Replace("!", "");
            strToReplace = strToReplace.Replace("@", "");
            strToReplace = strToReplace.Replace("#", "");
            strToReplace = strToReplace.Replace("$", "");
            strToReplace = strToReplace.Replace("%", "");
            strToReplace = strToReplace.Replace("^", "");
            strToReplace = strToReplace.Replace("&", "");
            strToReplace = strToReplace.Replace("*", "");
            strToReplace = strToReplace.Replace("(", "");
            strToReplace = strToReplace.Replace(")", "");

            strToReplace = strToReplace.Replace("_", "");
            strToReplace = strToReplace.Replace("+", "");
            strToReplace = strToReplace.Replace("=", "");
            strToReplace = strToReplace.Replace("[", "");
            strToReplace = strToReplace.Replace("]", "");
            strToReplace = strToReplace.Replace("{", "");
            strToReplace = strToReplace.Replace("}", "");
            strToReplace = strToReplace.Replace("|", "");
            strToReplace = strToReplace.Replace("\\", "");
            strToReplace = strToReplace.Replace(":", "");
            strToReplace = strToReplace.Replace(";", "");
            strToReplace = strToReplace.Replace("'", "");
            strToReplace = strToReplace.Replace("\"", "");
            strToReplace = strToReplace.Replace(".", "");
            strToReplace = strToReplace.Replace(",", "");
            strToReplace = strToReplace.Replace("<", "");
            strToReplace = strToReplace.Replace(">", "");
            strToReplace = strToReplace.Replace("''", "");
            strToReplace = strToReplace.Replace("?", "");
            strToReplace = strToReplace.Replace("/", "");
            strToReplace = strToReplace.Replace(" ", "-");
            strToReplace = strToReplace.Replace("  ", "-");
            return strToReplace.ToLower();
        }

        #region Exception Log       

        public static void GetExceptionLog(Exception exception, HttpRequest request)
        {
            //write error into file
            WriteToLogFile(exception.ToString());

            ////mail code starts here
            //string ErrorToEmail =  System.Configuration.ConfigurationManager.AppSettings["ErrorToEmail"] ;
            //string ErrorFromEmail = System.Configuration.ConfigurationManager.AppSettings["ErrorFromEmail"];
            //string ErrorBCCEmail = System.Configuration.ConfigurationManager.AppSettings["ErrorBCCEmail"];
            //string errorInfo = "";
            //string source = "";

            //// At this point we have information about the error
            //errorInfo = "<br>Offending URL: " + request.Url.ToString() + "<br>URL: " + exception.Source + "<br>Message: " + exception.Message + "<br>Stack trace: " + exception.StackTrace;
            //source = request.Url.ToString();

            //Error objError = new Error();

            //objError.IpAddress = request.UserHostAddress;
            //objError.ErrorDate = DateTime.Now.ToString();
            //objError.Browser = request.Browser.Version;
            //objError.UserName = request.LogonUserIdentity.IsAuthenticated ? request.LogonUserIdentity.Name : "Unknown";            
            //objError.Message = errorInfo;
            //objError.Source = source;

            //StringBuilder Mail = new StringBuilder();
            //Mail.Append("Date : " + objError.ErrorDate + "<br/>\n\r<br/>\n\r");
            //Mail.Append("Error Message : <br/>\n\r" + objError.Message + "<br/>\n\r<br/>\n\r");
            //Mail.Append("Error IP : " + objError.IpAddress + "<br/>\n\r<br/>\n\r");
            //Mail.Append("Error Host User Name : " + objError.UserName + "<br/>\n\r<br/>\n\r");
            
            //bool Chk = mail.SendMail(ErrorFromEmail, "PEDISMS Web Error", ErrorToEmail, "", Mail.ToString(), "PEDISMS Web Error", "", ErrorBCCEmail, true);
        }
        #endregion

        public static void WriteToLogFile(string LogMessage)
        {
            try
            {
                string baseDirectory = System.Configuration.ConfigurationManager.AppSettings["LogPath"].ToString();

                if (!Directory.Exists(baseDirectory))
                {
                    Directory.CreateDirectory(baseDirectory);
                }

                string filePath = baseDirectory + "\\" + DateTime.Today.ToString("yyyyMMMdd") + ".txt";
                string msg = String.Format("{0}:{1}", DateTime.Now, LogMessage);
                StreamWriter sw;
                if (!File.Exists(filePath))
                    sw = new StreamWriter(filePath);
                else
                    sw = File.AppendText(filePath);
                sw.WriteLine(msg);
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePassword(int minLength, int maxLength)
        {
            string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
            //string PASSWORD_CHARS_UCASE  = "ABCDEFGHJKLMNPQRSTWXYZ";
            string PASSWORD_CHARS_NUMERIC = "123456789";
            //string PASSWORD_CHARS_SPECIAL= "*$-+?_&=!%{}/";


            // Make sure that input parameters are valid.
            if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                return null;

            // Create a local array containing supported password characters
            // grouped by types. You can remove character groups from this
            // array, but doing so will weaken the password strength.
            char[][] charGroups = new char[][] 
        {
            PASSWORD_CHARS_LCASE.ToCharArray(),
            //PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
          //  PASSWORD_CHARS_SPECIAL.ToCharArray()
        };

            // Use this array to track the number of unused characters in each
            // character group.
            int[] charsLeftInGroup = new int[charGroups.Length];

            // Initially, all characters in each group are not used.
            for (int i = 0; i < charsLeftInGroup.Length; i++)
                charsLeftInGroup[i] = charGroups[i].Length;

            // Use this array to track (iterate through) unused character groups.
            int[] leftGroupsOrder = new int[charGroups.Length];

            // Initially, all character groups are not used.
            for (int i = 0; i < leftGroupsOrder.Length; i++)
                leftGroupsOrder[i] = i;

            // Because we cannot use the default randomizer, which is based on the
            // current time (it will produce the same "random" number within a
            // second), we will use a random number generator to seed the
            // randomizer.

            // Use a 4-byte array to fill it with random bytes and convert it then
            // to an integer value.
            byte[] randomBytes = new byte[4];

            // Generate 4 random bytes.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = (randomBytes[0] & 0x7f) << 24 |
                        randomBytes[1] << 16 |
                        randomBytes[2] << 8 |
                        randomBytes[3];

            // Now, this is real randomization.
            Random random = new Random(seed);

            // This array will hold password characters.
            char[] password = null;

            // Allocate appropriate memory for the password.
            if (minLength < maxLength)
                password = new char[random.Next(minLength, maxLength + 1)];
            else
                password = new char[minLength];

            // Index of the next character to be added to password.
            int nextCharIdx;

            // Index of the next character group to be processed.
            int nextGroupIdx;

            // Index which will be used to track not processed character groups.
            int nextLeftGroupsOrderIdx;

            // Index of the last non-processed character in a group.
            int lastCharIdx;

            // Index of the last non-processed group.
            int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

            // Generate password characters one at a time.
            for (int i = 0; i < password.Length; i++)
            {
                // If only one character group remained unprocessed, process it;
                // otherwise, pick a random character group from the unprocessed
                // group list. To allow a special character to appear in the
                // first position, increment the second parameter of the Next
                // function call by one, i.e. lastLeftGroupsOrderIdx + 1.
                if (lastLeftGroupsOrderIdx == 0)
                    nextLeftGroupsOrderIdx = 0;
                else
                    nextLeftGroupsOrderIdx = random.Next(0,
                                                         lastLeftGroupsOrderIdx);

                // Get the actual index of the character group, from which we will
                // pick the next character.
                nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

                // Get the index of the last unprocessed characters in this group.
                lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                // If only one unprocessed character is left, pick it; otherwise,
                // get a random character from the unused character list.
                if (lastCharIdx == 0)
                    nextCharIdx = 0;
                else

                    nextCharIdx = random.Next(0, lastCharIdx + 1);

                // Add this character to the password.
                password[i] = charGroups[nextGroupIdx][nextCharIdx];

                // If we processed the last character in this group, start over.
                if (lastCharIdx == 0)
                    charsLeftInGroup[nextGroupIdx] =
                                              charGroups[nextGroupIdx].Length;
                // There are more unprocessed characters left.
                else
                {
                    // Swap processed character with the last unprocessed character
                    // so that we don't pick it until we process all characters in
                    // this group.
                    if (lastCharIdx != nextCharIdx)
                    {
                        char temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] =
                                    charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }
                    // Decrement the number of unprocessed characters in
                    // this group.
                    charsLeftInGroup[nextGroupIdx]--;
                }

                // If we processed the last group, start all over.
                if (lastLeftGroupsOrderIdx == 0)
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                // There are more unprocessed groups left.
                else
                {
                    // Swap processed group with the last unprocessed group
                    // so that we don't pick it until we process all groups.
                    if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                    {
                        int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] =
                                    leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }
                    // Decrement the number of unprocessed groups.
                    lastLeftGroupsOrderIdx--;
                }
            }

            // Convert password characters into a string and return the result.
            return new string(password);

        }

        public string GenerateNumericPassword(int minLength, int maxLength)
        {            
            //string PASSWORD_CHARS_UCASE  = "ABCDEFGHJKLMNPQRSTWXYZ";
            string PASSWORD_CHARS_NUMERIC = "123456789";
            //string PASSWORD_CHARS_SPECIAL= "*$-+?_&=!%{}/";


            // Make sure that input parameters are valid.
            if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                return null;

            // Create a local array containing supported password characters
            // grouped by types. You can remove character groups from this
            // array, but doing so will weaken the password strength.
            char[][] charGroups = new char[][] 
        {            
            //PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
          //  PASSWORD_CHARS_SPECIAL.ToCharArray()
        };

            // Use this array to track the number of unused characters in each
            // character group.
            int[] charsLeftInGroup = new int[charGroups.Length];

            // Initially, all characters in each group are not used.
            for (int i = 0; i < charsLeftInGroup.Length; i++)
                charsLeftInGroup[i] = charGroups[i].Length;

            // Use this array to track (iterate through) unused character groups.
            int[] leftGroupsOrder = new int[charGroups.Length];

            // Initially, all character groups are not used.
            for (int i = 0; i < leftGroupsOrder.Length; i++)
                leftGroupsOrder[i] = i;

            // Because we cannot use the default randomizer, which is based on the
            // current time (it will produce the same "random" number within a
            // second), we will use a random number generator to seed the
            // randomizer.

            // Use a 4-byte array to fill it with random bytes and convert it then
            // to an integer value.
            byte[] randomBytes = new byte[4];

            // Generate 4 random bytes.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = (randomBytes[0] & 0x7f) << 24 |
                        randomBytes[1] << 16 |
                        randomBytes[2] << 8 |
                        randomBytes[3];

            // Now, this is real randomization.
            Random random = new Random(seed);

            // This array will hold password characters.
            char[] password = null;

            // Allocate appropriate memory for the password.
            if (minLength < maxLength)
                password = new char[random.Next(minLength, maxLength + 1)];
            else
                password = new char[minLength];

            // Index of the next character to be added to password.
            int nextCharIdx;

            // Index of the next character group to be processed.
            int nextGroupIdx;

            // Index which will be used to track not processed character groups.
            int nextLeftGroupsOrderIdx;

            // Index of the last non-processed character in a group.
            int lastCharIdx;

            // Index of the last non-processed group.
            int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

            // Generate password characters one at a time.
            for (int i = 0; i < password.Length; i++)
            {
                // If only one character group remained unprocessed, process it;
                // otherwise, pick a random character group from the unprocessed
                // group list. To allow a special character to appear in the
                // first position, increment the second parameter of the Next
                // function call by one, i.e. lastLeftGroupsOrderIdx + 1.
                if (lastLeftGroupsOrderIdx == 0)
                    nextLeftGroupsOrderIdx = 0;
                else
                    nextLeftGroupsOrderIdx = random.Next(0,
                                                         lastLeftGroupsOrderIdx);

                // Get the actual index of the character group, from which we will
                // pick the next character.
                nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

                // Get the index of the last unprocessed characters in this group.
                lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                // If only one unprocessed character is left, pick it; otherwise,
                // get a random character from the unused character list.
                if (lastCharIdx == 0)
                    nextCharIdx = 0;
                else

                    nextCharIdx = random.Next(0, lastCharIdx + 1);

                // Add this character to the password.
                password[i] = charGroups[nextGroupIdx][nextCharIdx];

                // If we processed the last character in this group, start over.
                if (lastCharIdx == 0)
                    charsLeftInGroup[nextGroupIdx] =
                                              charGroups[nextGroupIdx].Length;
                // There are more unprocessed characters left.
                else
                {
                    // Swap processed character with the last unprocessed character
                    // so that we don't pick it until we process all characters in
                    // this group.
                    if (lastCharIdx != nextCharIdx)
                    {
                        char temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] =
                                    charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }
                    // Decrement the number of unprocessed characters in
                    // this group.
                    charsLeftInGroup[nextGroupIdx]--;
                }

                // If we processed the last group, start all over.
                if (lastLeftGroupsOrderIdx == 0)
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                // There are more unprocessed groups left.
                else
                {
                    // Swap processed group with the last unprocessed group
                    // so that we don't pick it until we process all groups.
                    if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                    {
                        int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] =
                                    leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }
                    // Decrement the number of unprocessed groups.
                    lastLeftGroupsOrderIdx--;
                }
            }

            // Convert password characters into a string and return the result.
            return new string(password);

        }

        public string EnryptString(string strEncrypted)
        {
            try
            {
                byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(strEncrypted);
                string encryptedConnectionString = Convert.ToBase64String(b);
                return encryptedConnectionString;
            }
            catch
            {
                throw;
            }
        }

        public string DecryptString(string encrString)
        {
            string decryptedConnectionString = string.Empty;
            try
            {
                byte[] b = Convert.FromBase64String(encrString);
                decryptedConnectionString = System.Text.ASCIIEncoding.ASCII.GetString(b);

            }
            catch (Exception ex)
            {
                return decryptedConnectionString;

            }
            return decryptedConnectionString;
        }
    }
}
