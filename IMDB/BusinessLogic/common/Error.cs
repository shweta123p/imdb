﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using BusinessLogic.common;

namespace BusinessLogic
{
   public class Error
    {
        public string Message { get; set; }
        public string IpAddress { get; set; }
        public string Source { get; set; }
        public string UserName { get; set; }
        public string Browser { get; set; }
        public string ErrorDate { get; set; }
        public string ErrorId { get; set; }

        #region Insert Error 
        ///<author>Krunal Vyas</author>
        ///<date>04 May 2012</date>
        ///<summary>Insert Error</summary>
        ///<returns>true/false</returns>
        public static bool InsertError(Error objError)
        {
          
            Boolean status = false;
            try
            {
                Database objDLCommon = new Database();
                status = objDLCommon.InsertUpdateDeleteRow("InsertError", objError);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
        #endregion
       
        #region DeleteError
        public static Boolean DeleteError(object objerror)
        {
            Boolean status = false;
            try
            {
                Database objDLCommon = new Database();
                status = objDLCommon.InsertUpdateDeleteRow("DeleteError", objerror);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
        #endregion

        #region Get Producer List by ID

        public static DataTable GetErrorDetailById(string ErrorId)
        {
            DataTable dt = new DataTable();
            ParameterCollection parCollection = new ParameterCollection();
            Database objDLCommon = new Database();
            parCollection.Add(new Parameter("@ErrorId", DbType.String, ErrorId));
            dt = objDLCommon.ReturnDataTable("GetErrorDetailById", parCollection);
            return dt;
        }
        #endregion

       /*
        * Write to log file
        */
        public static  void WriteToLogFile(string LogMessage, string lStrDirectoryName)
        {
            try
            {
                // string lStrDirectoryName = "C";
                if (!(Directory.Exists(lStrDirectoryName)))
                {
                    Directory.CreateDirectory(lStrDirectoryName);
                }

                string lStrLogMessage = string.Empty;
                string lStrFileName = Convert.ToString(DateTime.Now.Day) + Convert.ToString(DateTime.Now.Month) + Convert.ToString(DateTime.Now.Year);
                string lStrLogFile = lStrDirectoryName + lStrFileName + ".log";
                StreamWriter SWLog;
                lStrLogMessage = string.Format("{0}:{1}", DateTime.Now, LogMessage);
                Console.WriteLine("MyLog: " + lStrLogMessage);
                if (!(File.Exists(lStrLogFile)))
                {
                    SWLog = new StreamWriter(lStrLogFile);
                }
                else
                {
                    SWLog = File.AppendText(lStrLogFile);
                }
                SWLog.WriteLine(lStrLogMessage);
                SWLog.WriteLine();
                SWLog.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

    }
}
