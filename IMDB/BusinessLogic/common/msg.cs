﻿using System;
using System.Web.UI;

namespace BusinessLogic
{
    public class msg
    {
        ///<author>kavit varma</author>        
        /// <summary>Generate Message</summary>        
        ///<returns> message </returns>
        /// <param name="msgFor"> message name</param>
        /// <param name="msgType">I - Inser, U- Update, D - Delete IE-Insert Error UE-Update Error, DE-Delete Error</param>
        public static void GetMsg(Page pg, string msgFor, string msgType, bool isAjaxPanelOnPage = false,bool closeOnconfirm=true,string secondAlterText="")
        {
            string strMsg = "";
            msgFor = msgFor.Replace("'", "");
            try
            {
                switch (msgType)
                {
                    case "I":
                        strMsg = msgFor + " Saved Successfully";
                        break;
                    case "U":
                        strMsg = msgFor + " Updated Successfully";
                        break;
                    case "P":
                        strMsg = msgFor + " Sent To Registered EmailId";
                        break;
                    case "D":
                        strMsg = msgFor + " Deleted Successfully";
                        break;
                    case "IP":
                        strMsg = msgFor + " Password Incorrect";
                        break;
                    case "IE":
                        strMsg = "Error In " + msgFor + " Save";
                        break;
                    case "UE":
                        strMsg = "Error In " + msgFor + " Update";
                        break;
                    case "DE":
                        strMsg = "Error In " + msgFor + " Delete";
                        break;
                    case "AE":
                        strMsg = msgFor + " Already Exist";
                        break;
                    case "CF":
                        strMsg = "Can not delete " + msgFor + " ,Child record found";
                        break;
                    //information 
                    case "IN":
                        strMsg = msgFor;
                        break;
                    case "Exception":
                        strMsg = msgFor;
                        break;
                    case "GE":
                        strMsg = "Error In " + " Getting" + msgFor;
                        break;
                    case "Negative":
                        strMsg = msgFor + " can not be negative or 0";
                        break;
                    default:
                        strMsg = msgFor;
                        break;
                }
                if (isAjaxPanelOnPage)
                //  pg.ClientScript.RegisterStartupScript(pg.GetType(), "CallMyFunction", "swal({   title: 'Success',   text:' "+strMsg+"',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false });", true);
                   ScriptManager.RegisterStartupScript(pg, pg.GetType(), "alert", "alert('" + strMsg + "');", true);
                else
                {
                    if(closeOnconfirm)
                    {
                        pg.ClientScript.RegisterStartupScript(pg.GetType(), "CallMyFunction", "swal({   title: 'Alert',   text:' " + strMsg + "',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false });", true);
                    }
                    else
                    {
                        pg.ClientScript.RegisterStartupScript(pg.GetType(), "CallMyFunction", "swal({ title: '', text: '" + strMsg + "',   type: 'success', confirmButtonText: 'OK',  closeOnConfirm: false,  showLoaderOnConfirm: true, }, function() { setTimeout(function() { "+secondAlterText+"});", true);
                    }
                }

                    
                  //  pg.ClientScript.RegisterStartupScript(pg.GetType(), "alert", "alert('" + strMsg + "');", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string gstinErrorMsg()
        {
            string msg;
            msg = "swal({  title: '', text: 'Please select Business & GSTIN.',   type: 'warning', confirmButtonText: 'OK',  closeOnConfirm: false,  showLoaderOnConfirm: true, }, function() { setTimeout(function() { location.href = '/default.aspx'}, 500); });";
            return msg;
        }
        public static string eWayBillErrorMsg()
        {
            string msg;
            msg = "swal({  title: '', text: 'Please make your e-Way Bill Registration!',   type: 'warning', confirmButtonText: 'OK',  closeOnConfirm: false,  showLoaderOnConfirm: true, }, function() { setTimeout(function() { location.href = '/eWayBill/eWayGSTINUser.aspx'}, 500); });";
            return msg;
        }
    }
}
