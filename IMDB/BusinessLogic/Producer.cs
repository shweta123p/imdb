﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace BusinessLogic
{
    public class Producer
    {
        public Boolean InsertProducer(string actorname, string gender, string bdate, string bio)
        {
            Database objDLCommon = new Database();
            Boolean status = false;
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@name", DbType.String, actorname.ToString()));
                pm.Add(new Parameter("@gender", DbType.String, gender.ToString()));
                pm.Add(new Parameter("@dateofBirth", DbType.String, bdate.ToString()));
                pm.Add(new Parameter("@bio", DbType.String, bio.ToString()));

                status = objDLCommon.InsertUpdateDeleteRow("InsertProducer", null, pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public Boolean UpdateProducer(string producerId, string actorname, string gender, string bdate, string bio)
        {
            Database objDLCommon = new Database();
            Boolean status = false;
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@producerId", DbType.String, producerId.ToString()));
                pm.Add(new Parameter("@name", DbType.String, actorname.ToString()));
                pm.Add(new Parameter("@gender", DbType.String, gender.ToString()));
                pm.Add(new Parameter("@dateofBirth", DbType.String, bdate.ToString()));
                pm.Add(new Parameter("@bio", DbType.String, bio.ToString()));

                status = objDLCommon.InsertUpdateDeleteRow("UpdateProducer", null, pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public void FillProducer(DropDownList objDrp)
        {
            try
            {
                Database objDLCommon = new Database();
                objDLCommon.FillingDropDown("FillProducer", objDrp, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ProducerList()
        {
            DataTable dt = new DataTable();
            try
            {
                Database objDLCommon = new Database();
                dt = objDLCommon.ReturnDataTable("ProducerList", null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
