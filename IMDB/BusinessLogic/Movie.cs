﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace BusinessLogic
{
    public class Movie
    {
        public Boolean InsertMovieDetail(string movieName, string yearOfRelease, string ploat, string posterPath, string producerId, string actors)
        {
            Database objDLCommon = new Database();
            Boolean status = false;
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@movieName", DbType.String, movieName.ToString()));
                pm.Add(new Parameter("@yearOfRelease", DbType.String, yearOfRelease.ToString()));
                pm.Add(new Parameter("@ploat", DbType.String, ploat.ToString()));
                pm.Add(new Parameter("@posterPath", DbType.String, posterPath.ToString()));
                pm.Add(new Parameter("@producerId", DbType.String, producerId.ToString()));
                pm.Add(new Parameter("@actorId", DbType.String, actors.ToString()));

                status = objDLCommon.InsertUpdateDeleteRow("InsertMovieDetail", null, pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public Boolean UpdateMovieDetail(string movieId, string movieName, string yearOfRelease, string ploat, string producerId, string actors)
        {
            Database objDLCommon = new Database();
            Boolean status = false;
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@movieId", DbType.String, movieId.ToString())); 
                pm.Add(new Parameter("@movieName", DbType.String, movieName.ToString()));
                pm.Add(new Parameter("@yearOfRelease", DbType.String, yearOfRelease.ToString()));
                pm.Add(new Parameter("@ploat", DbType.String, ploat.ToString()));
                //pm.Add(new Parameter("@posterPath", DbType.String, posterPath.ToString()));
                pm.Add(new Parameter("@producerId", DbType.String, producerId.ToString()));
                pm.Add(new Parameter("@actorId", DbType.String, actors.ToString()));

                status = objDLCommon.InsertUpdateDeleteRow("UpdateMovieDetail", null, pm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
        public DataSet GetMovieDetails()
        {
            Database objDLCommon = new Database();
            DataSet ds = new DataSet();
            try
            {
                ds = objDLCommon.ReturnDataSet("GetMovieDetails",null, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet GetMovieDetailsbyId(string movieId)
        {
            Database objDLCommon = new Database();
            DataSet ds = new DataSet();
            try
            {
                ParameterCollection pm = new ParameterCollection();
                pm.Add(new Parameter("@MovieId", DbType.String, movieId.ToString()));
                ds = objDLCommon.ReturnDataSet("GetMovieDetailsbyId",pm , null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        
    }
}
