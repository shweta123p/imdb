﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IMDB
{
    public partial class Movies : System.Web.UI.Page
    {
        BusinessLogic.Actor objactor = new BusinessLogic.Actor();
        BusinessLogic.Producer objProducer = new BusinessLogic.Producer();
        BusinessLogic.Movie objMovie = new BusinessLogic.Movie();
        static string movieId = "";
        static string producerId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                objactor.FillActor(ddlactor);
                objProducer.FillProducer(ddlproducer);

                if (Request["movieId"] != null)
                {
                    movieId = Request["movieId"].ToString();
                }

                if (!String.IsNullOrEmpty(movieId) && !(movieId.Equals("")))
                {
                    DataSet ds = objMovie.GetMovieDetailsbyId(movieId.ToString());

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtMDetails = ds.Tables[0];
                            DataTable dtADetails = ds.Tables[1];

                            if (dtMDetails.Rows.Count > 0)
                            {
                                txtMovieName.Text = dtMDetails.Rows[0]["movieName"].ToString();
                                txtyearofrelease.Text = dtMDetails.Rows[0]["yearOfRelease"].ToString();
                                ddlproducer.SelectedValue = dtMDetails.Rows[0]["producerId"].ToString();
                                txtplot.Text = dtMDetails.Rows[0]["ploat"].ToString();
                                producerId = dtMDetails.Rows[0]["producerId"].ToString();

                                uploadlicence.Visible = true;
                                fuupload.Visible = false;
                                viewPoster.Visible = true;
                                reqreconcile.Enabled = false;

                                string path = dtMDetails.Rows[0]["posterPath"].ToString();
                                uploadlicence.Value = path.Split('/').Last();

                                linkuploadlicence.NavigateUrl = "~/" + path;
                                linkuploadlicence.Width = 100;

                            }
                            if (dtADetails.Rows.Count > 0)
                            {
                                rptActorDetails.DataSource = dtADetails;
                                rptActorDetails.DataBind();
                            }

                        }
                        btnsave.Text = "Update Movie";
                    }
                    else
                    {
                        viewPoster.Visible = false;
                    }
                }
            }
        }

        protected void rptActorDetails_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                int id = Convert.ToInt32(e.CommandArgument);

                DataTable dt = GetDTfromGrids();
                int totalCnt = dt.Rows.Count;

                for (int j = id; j < totalCnt; j++)
                {
                    dt.Rows[j]["SrNo"] = j;
                }
                id = id - 1;
                dt.Rows[id].Delete();
                dt.AcceptChanges();
                rptActorDetails.DataSource = dt;
                rptActorDetails.DataBind();
            }

            objactor.FillActor(ddlactor);
            objProducer.FillProducer(ddlproducer);

            if (movieId != "")
            {
                ddlproducer.SelectedValue = producerId;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlactor.SelectedValue != "0")
                {
                    DataTable dtActor = GetDTfromGrids();

                    DataRow newDtRow = dtActor.NewRow();

                    DataTable dtactorlist = objactor.ActorListById(ddlactor.SelectedValue);

                    if (dtactorlist.Rows.Count > 0)
                    {
                        newDtRow["SrNo"] = rptActorDetails.Items.Count + 1;
                        newDtRow["actorId"] = ddlactor.SelectedValue;
                        newDtRow["name"] = dtactorlist.Rows[0]["name"].ToString();
                        newDtRow["gender"] = dtactorlist.Rows[0]["gender"].ToString();
                        newDtRow["dateOfBirth"] = dtactorlist.Rows[0]["dateOfBirth"].ToString();
                        newDtRow["bio"] = dtactorlist.Rows[0]["bio"].ToString();
                    }

                    HiddenField hfid = new HiddenField();
                    hfid.Value = ddlactor.SelectedValue;

                    //--------------------------------- CHECK FOR DUPLICACY -------------------------------------------------------------

                    string rowFilter = "actorId='" + ddlactor.SelectedValue + "'";

                    DataRow[] sameSlab = dtActor.Select(rowFilter);

                    if (sameSlab.Length > 0)
                    {
                        BusinessLogic.msg.GetMsg(this.Page, "Actor ", "AE", true);
                    }
                    else
                    {
                        dtActor.Rows.Add(newDtRow);
                    }
                    rptActorDetails.DataSource = dtActor;
                    rptActorDetails.DataBind();

                    objactor.FillActor(ddlactor);
                    objProducer.FillProducer(ddlproducer);

                    if (movieId != "")
                    {
                        ddlproducer.SelectedValue = producerId;
                    }

                    ddlactor.SelectedValue = hfid.Value;
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "sweetAlert('Oops...', 'Please Select Actor!', 'error');", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception", true);
                BusinessLogic.Error.WriteToLogFile("Error On btnAdd_Click in Movies.aspx  : " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string actors = "";
                string path = "";

                if (btnsave.Text == "Save Movie")
                {
                    foreach (RepeaterItem item in rptActorDetails.Items)
                    {
                        actors += (item.FindControl("lblactorid") as Label).Text + ",";
                    }

                    if (rptActorDetails.Items.Count > 0)
                    {
                        rfvforactor.Enabled = false;
                    }

                    if (fuupload.HasFile)
                    {
                        String[] allowedExtensions = { ".jpeg", ".jpg", ".png", ".pdf" };

                        if (checkExtension(allowedExtensions, fuupload))
                        {

                            lblfile.Text = "not a valid file";
                            lblfile.Visible = true;
                            return;
                        }
                        if (fuupload.PostedFile.ContentLength > Convert.ToInt64(ConfigurationManager.AppSettings["filesize"]))
                        {
                            lblfile.Text = "Please upload file less than " + (Convert.ToInt64(ConfigurationManager.AppSettings["filesize"]) / 1000).ToString() + "kb.";
                            BusinessLogic.msg.GetMsg(this.Page, "Please upload file less than " + (Convert.ToInt64(ConfigurationManager.AppSettings["filesize"]) / 1000).ToString() + "kb.", "Exception");
                            lblfile.Visible = true;
                            return;
                        }
                        string fileName = fuupload.PostedFile.FileName;
                        path = uploadFile(fuupload, fileName, txtMovieName.Text.Trim());

                        if (path == "-1")
                        {
                            return;
                        }
                    }

                    Boolean status = objMovie.InsertMovieDetail(txtMovieName.Text.Trim(), txtyearofrelease.Text.Trim(), txtplot.Text.Trim(), path, ddlproducer.SelectedValue, actors);

                    if (status == true)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallMyFunction", "swal({ title: 'Success',   text: 'Movie Details Added Successfully..',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false },function(){window.location='/Movies.aspx';});", true);
                        Clear();
                    }
                }
                else if (btnsave.Text == "Update Movie")
                {
                    foreach (RepeaterItem item in rptActorDetails.Items)
                    {
                        actors += (item.FindControl("lblactorid") as Label).Text + ",";
                    }

                    if (rptActorDetails.Items.Count > 0)
                    {
                        rfvforactor.Enabled = false;
                    }
                    Boolean status = objMovie.UpdateMovieDetail(movieId, txtMovieName.Text.Trim(), txtyearofrelease.Text.Trim(), txtplot.Text.Trim(), ddlproducer.SelectedValue, actors);

                    if (status == true)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallMyFunction", "swal({ title: 'Success',   text: 'Movie Details Updated Successfully..',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false },function(){window.location='/Default.aspx';});", true);
                        Clear();
                    }
                }

            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception", true);
                BusinessLogic.Error.WriteToLogFile("Error On btnsave_Click in Movies.aspx  : " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        protected void btnViewMovie_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/default.aspx", false);
                movieId = "";
                producerId = "";
                Clear();
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception", true);
                BusinessLogic.Error.WriteToLogFile("Error On btnViewMovie_Click in Movies.aspx  : " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        public void Clear()
        {
            txtMovieName.Text = string.Empty;
            txtyearofrelease.Text = string.Empty;
            txtplot.Text = string.Empty;
            ddlactor.SelectedValue = "0";
            ddlproducer.SelectedValue = "0";
            fuupload.Visible = true;
            uploadlicence.Visible = false;
            viewPoster.Visible = false;
            reqreconcile.Enabled = true;
            linkuploadlicence.Visible = false;

            rfvforactor.Enabled = true;
            rfvforddlproducer.Enabled = true;

            rptActorDetails.DataSource = null;
            rptActorDetails.DataBind();
        }

        public DataTable GetDTfromGrids()
        {
            DataTable dtService = new DataTable();
            dtService.Columns.Add("SrNo", typeof(string));
            dtService.Columns.Add("actorId", typeof(string));
            dtService.Columns.Add("name", typeof(string));
            dtService.Columns.Add("gender", typeof(string));
            dtService.Columns.Add("dateOfBirth", typeof(string));
            dtService.Columns.Add("bio", typeof(string));


            foreach (RepeaterItem item in rptActorDetails.Items)
            {
                DataRow dr = dtService.NewRow();
                Label lblSrNo = (Label)item.FindControl("lblSrNo");
                dr["SrNo"] = lblSrNo.Text;
                Label lblFromQty = (Label)item.FindControl("lblactorid");
                dr["actorId"] = lblFromQty.Text;
                Label lblToQty = (Label)item.FindControl("lblname");
                dr["name"] = lblToQty.Text;
                Label lblDisplayNo = (Label)item.FindControl("lblgender");
                dr["gender"] = lblDisplayNo.Text;
                Label lblService = (Label)item.FindControl("lbldob");
                dr["dateOfBirth"] = lblService.Text;
                Label lblbio = (Label)item.FindControl("lblbio");
                dr["bio"] = lblbio.Text;

                dtService.Rows.Add(dr);
            }

            return dtService;
        }


        public bool checkExtension(String[] allowedExtensions, FileUpload myFileUpload)
        {
            bool fileOK = true;
            try
            {
                String fileExtension = System.IO.Path.GetExtension(myFileUpload.FileName).ToLower();

                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fileOK;

        }

        public string uploadFile(FileUpload ftpdocumentupload, string filename, string uniqueName)
        {
            String path = Server.MapPath("~/Poster/" + uniqueName + "/");
            string tsFileName = "";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            try
            {
                tsFileName = System.IO.Path.GetFileNameWithoutExtension(filename) + '_' + DateTime.Now.ToString("yyyyMMddHHmmssfff") + System.IO.Path.GetExtension(ftpdocumentupload.FileName).ToLower();
                ftpdocumentupload.PostedFile.SaveAs(path + tsFileName);

            }
            catch (Exception ex)
            {
                return "-1";
            }
            finally
            {
            }
            return "/Poster/" + uniqueName + "/" + tsFileName;
        }

    }
}