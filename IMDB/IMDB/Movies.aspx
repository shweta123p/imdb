﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Movies.aspx.cs" Inherits="IMDB.Movies" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="white-box p-l-0 p-r-0 listtable">
        <div class="row">
            <div class="col-md-12">
                <section id="section-line-1">
                    <div class="white-box bg-light m-b-0 clearfix">
                        <div class="pull-left">
                            <h2 class="m-b-0 m-t-0">
                                <asp:Label class="control-label" ID="lbladdMovie" Text="Add Movie" runat="server"></asp:Label>
                            </h2>
                        </div>
                        <div class="pull-right">
                            <asp:LinkButton runat="server" ID="btnViewMovie" OnClick="btnViewMovie_Click" class="btn btn-info waves-effect waves-light" role="button" Text="&lt;i class=&quot;icon-cloud-upload m-r-5&quot;&gt;&lt;/i&gt;&lt;span&gt;View Movie List&lt;/span&gt;"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="white-box m-b-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>
                                        <asp:Label class="control-label" ID="lblMovieName" Text="Movie Name" runat="server"></asp:Label></b>
                                    <asp:TextBox runat="server" ID="txtMovieName" MaxLength="200" CssClass="form-control" placeholder="Movie Name" TabIndex="1"></asp:TextBox>
                                    <div>
                                        <asp:RequiredFieldValidator ID="rfvmoviename" Font-Size="12px" runat="server" ErrorMessage="*Please Enter Movie Name" Display="Dynamic"
                                            ControlToValidate="txtMovieName" ValidationGroup="save" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Font-Size="12px" ID="revmoviename" runat="server" ControlToValidate="txtMovieName" ValidationGroup="save"
                                            ForeColor="Red" ValidationExpression="[a-zA-Z0-9\-_ /]*$" ErrorMessage="Invalid Movie Name" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>
                                        <asp:Label ID="lblyearofrelease" runat="server" class="control-label" Text="Year of Release"></asp:Label></b>
                                    <div class="input-group">
                                        <asp:TextBox runat="server" ID="txtyearofrelease" AutoCompleteType="Disabled" class="form-control mydatepicker1" placeholder="dd/mm/yyyy"></asp:TextBox>
                                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                    <div>
                                        <asp:RequiredFieldValidator Font-Size="12px" ID="rfvfortxtyearofrelease" runat="server" ErrorMessage="*Please Enter Year of Release"
                                            Display="Dynamic" ControlToValidate="txtyearofrelease" ValidationGroup="save" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Font-Size="12px" ID="revtxtyearofrelease" runat="server" ControlToValidate="txtyearofrelease" ValidationGroup="save"
                                            ForeColor="Red" ValidationExpression="[0-9/]*$" ErrorMessage="Enter Valid release year" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <b>
                                        <asp:Label ID="lblactor" runat="server" class="control-label" Text="Actor"></asp:Label></b>
                                    <asp:DropDownList ID="ddlactor" runat="server" class="form-control select2" TabIndex="3"></asp:DropDownList>

                                    <div>
                                        <asp:RequiredFieldValidator ID="rfvforactor" runat="server" ErrorMessage="*Please Select Actor" Font-Size="12px"
                                            Display="Dynamic" ControlToValidate="ddlactor" ValidationGroup="save" InitialValue="0" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group" style="padding-top: 25px;">
                                    <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnAdd_Click"><i class="fa fa-plus"></i></asp:LinkButton>
                                    <a href="javascript:void(0)" class="btn btn-danger cst-btn triggeraddnew btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="icon-cloud-upload m-r-5"></i><span>
                                        <asp:Label ID="lblAddActor" runat="server" Text="Add Actor"></asp:Label></span></a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <b>
                                        <asp:Label ID="lblproducer" runat="server" class="control-label" Text="Producer"></asp:Label></b>
                                    <asp:DropDownList ID="ddlproducer" runat="server" class="form-control select2" TabIndex="4"></asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator ID="rfvforddlproducer" runat="server" ErrorMessage="*Please Select Producer" Font-Size="12px"
                                            Display="Dynamic" ControlToValidate="ddlproducer" ValidationGroup="save" InitialValue="0" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group" style="padding-top: 25px;">
                                    <a href="javascript:void(0)" class="btn btn-danger cst-btn triggeraddnew btn-sm" data-toggle="modal" data-target="#addnewproducer" data-whatever="@mdo"><i class="icon-cloud-upload m-r-5"></i><span>
                                        <asp:Label ID="lblAddProducer" runat="server" Text="Add Producer"></asp:Label></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="white-box p-l-0 p-r-0 listtable">
                                    <asp:Repeater ID="rptActorDetails" runat="server" OnItemCommand="rptActorDetails_OnItemCommand">
                                        <HeaderTemplate>
                                            <div class="white-box p-l-0 p-r-0 listtable">
                                                <table id="tblActor" class="table table-striped" style="margin: 0 !important; width: 100% !important;">
                                                    <thead>
                                                        <tr>
                                                            <th style="display: none">Sr No.
                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblthActor" runat="server" Text="Actor"></asp:Label>
                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblthgender" runat="server" Text="Gender"></asp:Label>
                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblthbdate" runat="server" Text="BithDate"></asp:Label>
                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblthbio" runat="server" Text="Bio"></asp:Label>
                                                            </th>
                                                            <th>Delete</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr id="trHeader" runat="server">
                                                <td style="display: none" runat="server">
                                                    <asp:Label ID="lblactorid" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"actorId") %>' Visible="false"></asp:Label>
                                                    <asp:Label runat="server" ID="lblSrNo" Text="<%# Container.ItemIndex + 1 %>"></asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <asp:Label ID="lblname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"name") %>'></asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <asp:Label ID="lblgender" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"gender") %>'></asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <asp:Label ID="lbldob" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"dateOfBirth") %>'></asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <asp:TextBox ID="lblbio" TextMode="MultiLine" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"bio") %>' ReadOnly="true" Width="300" Height="75"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="btnActorDelete" CssClass="label label-danger" CommandName="Delete" CommandArgument='<%# Container.ItemIndex + 1 %>'><i class="fa fa-remove"></i></asp:LinkButton></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody> </table></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <b>
                                        <asp:Label ID="lblplot" runat="server" class="control-label" Text="Plot"></asp:Label></b>
                                    <asp:TextBox runat="server" MaxLength="1000" ID="txtplot" class="form-control" Rows="5" TextMode="MultiLine"></asp:TextBox>

                                    <div>
                                        <asp:RequiredFieldValidator ID="rfvplot" runat="server" ErrorMessage="*Please Enter Plot" Font-Size="12px"
                                            Display="Dynamic" ControlToValidate="txtplot" ValidationGroup="save" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group" id="uploadFileDiv">
                                    <asp:Label class="control-label" ID="lblChooseFile" Text="Choose file from your computer" runat="server"></asp:Label>
                                    <asp:FileUpload ID="fuupload" runat="server" Width="211px" TabIndex="6"
                                        onblur="checkName(this);" CssClass="dropify" data-max-file-size="2M" data-allowed-file-extensions="jpg jpeg png gif" />
                                    <input type="text" class="form-control" readonly="" runat="server" id="uploadlicence" visible="false" />
                                    <asp:CustomValidator ErrorMessage="*only JPG/PNG/GIF files are supported" ValidationGroup="checkfileupload" ForeColor="Red" Font-Size="12px"
                                        ControlToValidate="fuupload" runat="server" ClientValidationFunction="CheckFile"></asp:CustomValidator>

                                    <span class="help-block">JPG/PNG/GIF files are supported</span>

                                    <asp:RequiredFieldValidator ID="reqreconcile" runat="server" ControlToValidate="fuupload"
                                        ValidationGroup="save" ErrorMessage="Please upload file" Display="None"></asp:RequiredFieldValidator>

                                    <cc1:ValidatorCalloutExtender ID="vcereconcile" runat="server" TargetControlID="reqreconcile" BehaviorID="vcereconcile">
                                    </cc1:ValidatorCalloutExtender>

                                    <asp:Label ID="lblfile" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-3" id="viewPoster" runat="server" style="padding-top: 20px;" visible="false">
                                <asp:HyperLink ID="linkuploadlicence" runat="server" Text="View" Target="_blank" class="btn btn-outline btn-danger btn-rounded dropdown-toggle waves-effect waves-light"
                                    Visible="true"> </asp:HyperLink>
                            </div>
                        </div>
                    </div>
                    <hr class="m-0" />
                    <div class="white-box m-b-0">
                        <asp:Button ID="btnsave" runat="server" ValidationGroup="save" Text="Save Movie" class="btn btn-success" OnClick="btnsave_Click" TabIndex="7" />
                    </div>
                    <br />
                    <br />
                </section>
            </div>
        </div>
        <asp:HiddenField ID="hfMovieId" runat="server" />
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">

    <script type="text/javascript">      

        $(document).ready(function () {

            $('.mydatepicker1, #datepicker').datepicker({
                format: 'yyyy',
                viewMode: "years",
                minViewMode: "years",
                orientation: 'bottom auto',
            })

            $('.mydatepicker, #datepicker').datepicker({
                format: 'dd/mm/yyyy',
                orientation: 'bottom auto',
            })

            $('.select2').select2();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        });

        function EndRequestHandler(sender, args) {
            $('.mydatepicker1').datepicker(
                {
                    format: 'yyyy',
                    viewMode: "years",
                    minViewMode: "years",
                    orientation: 'bottom auto',
                });

            $('.mydatepicker, #datepicker').datepicker({
                format: 'dd/mm/yyyy',
                orientation: 'bottom auto',
            })

            $('.select2').select2();

        }
    </script>
</asp:Content>
