﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GSP._Default" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/toastr.min.css" rel="stylesheet">
    <style>
        .tabs-style-line.purchasetab nav li.tab-current a {
            box-shadow: inset 0 -2px #00c292;
            color: #00c292;
        }

        .tabs-style-line nav a {
            font-weight: 400;
        }

        .timeline > li {
            position: relative !important;
            left: auto !important;
            top: auto !important;
        }

        .element-item {
            -webkit-transition: all 1s ease-in-out;
            -moz-transition: all 1s ease-in-out;
            -o-transition: all 1s ease-in-out;
            transition: all 1s ease-in-out;
        }

        .table tr th.invtype {
            background-color: #fff3cd;
        }

            .table tr th.invtype select {
                background-color: #fff3cd;
                border: 0px none;
            }

        td.col-date {
            width: 100px !important;
        }

        td.col-status {
            width: 150px !important;
        }

        td.col-message {
            width: 300px !important;
        }

        td.col-view {
            width: 60px !important;
        }

        td.col-sizep, th.col-sizep {
            width: 10% !important;
        }

        td.col-msgp, t4.col-msgp {
            width: 40% !important;
        }
    </style>

</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="ajax-loading" style="display: none">
        <div class="loader">Loading...</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box bg-light m-b-0 clearfix">
                <div class="pull-left">
                    <h2 class="m-b-0 m-t-0">
                        <asp:Label ID="lblUploadSummary" runat="server" Text="Movie Details"></asp:Label>
                    </h2>
                </div>
                <div class="pull-right">
                    <asp:LinkButton runat="server" ID="btnAddMovie" OnClick="btnAddMovie_Click" class="btn btn-info waves-effect waves-light" role="button" Text="&lt;i class=&quot;icon-cloud-upload m-r-5&quot;&gt;&lt;/i&gt;&lt;span&gt;Add Movie&lt;/span&gt;"></asp:LinkButton>
                </div>
            </div>
            <div class="white-box m-b-0">
                <h5><strong>
                    <asp:Label ID="lblUploadHistory" runat="server" Text="Movie List"></asp:Label>
                </strong>
                </h5>
            </div>
            <div class="table-responsive listtable white-box p-0">
                <asp:Repeater ID="repMovieList" runat="server" OnItemCommand="repMovieList_OnItemCommand">
                    <HeaderTemplate>
                        <table id="tbluploaderrorlog" class="table table-hover color-table muted-table table-bordered ">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:Label ID="lblMovieId" runat="server" Text="Movie Id"></asp:Label>
                                    </th>
                                    <th class="text-center">
                                        <asp:Label ID="lblMname" runat="server" Text="Movie"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="lblYearofRelease" runat="server" Text="Year of Release"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="lblPlot" runat="server" Text="Plot"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="lblView" runat="server" Text="View"></asp:Label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-center col-date"><%#  DataBinder.Eval(Container.DataItem,"movieId") %></td>
                            <td class="text-center col-date"><%#  DataBinder.Eval(Container.DataItem,"movieName") %></td>
                            <td class="text-center col-status"><%#  DataBinder.Eval(Container.DataItem,"Producer") %></td>
                            <td class="text-center col-status"><%#  DataBinder.Eval(Container.DataItem,"yearOfRelease") %></td>
                            <td class="text-center col-date"><%#  DataBinder.Eval(Container.DataItem,"ploat") %></td>
                            <td class="text-center col-view">
                                <asp:Button ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                    Enabled="true" ImageUrl="/images/edit.png" class="fontsize18 btn text-success btn-sm btn-default"
                                    Text="&#9998;" CommandArgument='<%# Eval("movieId") %>' />
                            </td>
                        </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>  </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <%--  <script type="text/javascript" src="../js/toastr.min.js"></script>
    <script type="text/javascript" src="../js/isotope.pkgd.min.js"></script>
    <script src="../js/cbpFWTabs.js"></script>
    <script type="text/javascript">

        new CBPFWTabs(document.getElementById('tabs'));

        $(document).ready(function () {

            // filter items on button click
            $('.filter-button-group').on('click', 'a', function () {
                $('.filter-button-group a').removeClass("btn-inverse").addClass("btn-default");
                $(this).removeClass("btn-default").addClass("btn-inverse");
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({ filter: filterValue });
            });

            $('.fillGSTR1').click(function (e) {
                e.preventDefault();
                $('#gerstarted')[0].click();
            });

            var myTable = $('#myTable').DataTable({
                "processing": true,
                "scrollX": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "cache": false,
                dom: 'Bfrtip',
                buttons: ['pageLength',
                    {
                        extend: 'collection',
                        text: '<i class="fa fa-file-excel-o"></i> Export Invoices',
                        buttons: [
                            {
                                extend: 'excel',
                                exportOptions: {
                                    format: {
                                        header: function (data, column, row) {
                                            if (column === 0)
                                                return "Invoice Type";
                                            else
                                                return data;
                                        }
                                    }
                                    //columns: ':visible:not(.not-export-col)'
                                }
                            }
                        ]
                    }
                ],
                language: {
                    buttons: {
                        pageLength: {
                            _: "<i class='fa fa-list-ul'></i> Show %d Rows",
                            '-1': "<i class='fa fa-list-ul'></i> Show All Rows"
                        }
                    }
                },
                "aoColumnDefs": [
                    {
                        "targets": 0, orderable: false,
                    }
                ],
                "order": [],
                initComplete: function () {
                    this.api().columns(0).every(function () {
                        var column = this;
                        var select = $('<select class="dtsearch"><option value="">Invoice Type</option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append("<option value='" + d + "'>" + d + "</option>")
                        });
                    });
                }
            })

            $('.gstrsummary table tr td').on('click', 'a.btnviewinvoice', function (e) {
                e.preventDefault();
                $('#section3')[0].click();
                var dataItem = $(this).attr("data-item");
                var myElement = $(".dtsearch");
                myElement.val(dataItem);
                myTable.column(0).search(dataItem).draw();
                //var filteredData = myTable
                //    .columns(2)
                //    .data()
                //    .flatten()
                //    .filter(function (value, index) {
                //        return value == dataItem ? true : false;
                //    });
            });

            init_masonry();

            var tbluploaderrorlog = $('#tbluploaderrorlog').DataTable({
                "processing": true,
                "searching": false,
                "bInfo": false,
                "cache": false,
                "ordering": false,
                "bLengthChange": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData[0]);
                },
                "aoColumnDefs": [
                    { 'bSearchable': false, 'bVisible': false, 'aTargets': [0] },
                    { width: 200, targets: 1 },
                    {
                        "targets": 2,
                        "render": function (data, type, row) {
                            if (data == "Processed With Error")
                                data = "<span class='label label-warning font-12'>" + data + "</span>";
                            else if (data == "Processed")
                                data = "<span class='label label-success font-12'>" + data + "</span>";
                            else if (data == "Error")
                                data = "<span class='label label-danger font-12'>" + data + "</span>";
                            return data;
                        }
                    },
                    { "targets": -1, orderable: false, "data": null, "defaultContent": "<button type='button' data-toggle='modal' data-target='#exampleModal2' data-whatever='@mdo'data-toggle='tooltip' data-original-title='View Log' class='btn btn-default btn-circle btnuploadlogView'><i class='fa fa-list-alt text-success'></i></button>" }
                ],

                "order": [[0, "desc"]]

            });

            $("#tbluploaderrorlog tbody").on('click', '.btnuploadlogView', function (e) {
                e.preventDefault();
                $('.ajax-loading-model').fadeIn();
                var data = tbluploaderrorlog.row($(this).parents('tr')).data();
                var rowId = data[0];
                $(".modal .lblorg").text(data[1]);
                $(".modal .lblasset").text(data[2]);
                var exportinvoice;
                //resetmodelform();
                $.ajax({
                    type: "POST",
                    url: "gstr1.aspx/GetGSTR1uploadStatusById",
                    cache: false,
                    data: "{APIUploadId: '" + rowId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if ($.fn.DataTable.isDataTable('#tblerror')) {
                            $('#tblerror').DataTable().destroy();
                        }
                        $('#tblerror tbody').empty();
                        exportinvoice = $('#tblerror').DataTable({
                            "data": JSON.parse(response.d),
                            "processing": true,
                            "bPaginate": false,
                            "bInfo": false,
                            "searching": false,
                            "cache": false,
                            "scrollX": true,
                            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                                nRow.setAttribute('id', aData.ReturnInvId);
                                //var cellValue = aData.isUploadedToGSTIN; 
                                //if (cellValue == "Not Uploaded")
                                //    $('td:eq(2)', nRow).html("<span class='label label-danger font-12'>" + cellValue + "</span>");
                                //else if (cellValue == "Uploaded")
                                //    $('td:eq(2)', nRow).html("<span class='label label-success font-12'>" + cellValue + "</span>");                                                       
                            },
                            "aoColumns": [
                                { "mData": "ReturnInvId" },
                                { "mData": "APIUploadId" },
                                { "mData": "isUploadedToGSTIN" },
                                { "mData": "errorMsg", "sClass": "" },
                                { "mData": "invBusinessType", "sClass": "col-sizep" },
                                { "mData": "returnPeriod", "sClass": "col-sizep" },
                                { "mData": "inum", "sClass": "col-sizep" },
                                { "mData": "idt", "sClass": "col-sizep" }

                            ],
                            "aoColumnDefs": [
                                { 'bSearchable': false, 'bVisible': false, 'aTargets': [0] },
                                { 'bSearchable': false, 'bVisible': false, 'aTargets': [1] },
                                { 'bSearchable': true, 'bVisible': false, 'aTargets': [2] },
                                {
                                    "targets": 5,
                                    "render": function (data, type, row) {
                                        data = "<span class='label label-default font-12'>" + data + "</span>";
                                        return data;
                                    }
                                }
                            ],

                            "drawCallback": function (settings) {
                                var api = this.api();
                                var rows = api.rows({ page: 'current' }).nodes();
                                var last = null;
                                api.column(2, { page: 'current' }).data().each(function (group, i) {
                                    if (last !== group) {
                                        if (group == "Not Uploaded")
                                            $(rows).eq(i).before('<tr class="group"><td colspan="5" style="background-color:#fff3cd"><b><span class="label label-danger font-12">' + group + '</span></b></td></tr>');
                                        else if (group == "Uploaded")
                                            $(rows).eq(i).before('<tr class="group"><td colspan="5" style="background-color:#fff3cd"><b><span class="label label-success font-12">' + group + '</span></b></td></tr>');

                                        last = group;
                                    }
                                });
                            },
                            "order": [[0, "desc"]]
                        });
                        // Order by the grouping
                        $('#tblerror tbody').on('click', 'tr.group', function () {
                            var currentOrder = exportinvoice.order()[0];
                            if (currentOrder[0] === 2 && currentOrder[1] === 'desc') {
                                exportinvoice.order([2, 'desc']).draw();
                            }
                            else {
                                exportinvoice.order([2, 'asc']).draw();
                            }
                        });

                    },
                    failure: function (response) {
                        //$('.ajax-loading-model').fadeOut();	
                    },
                    complete: function () {
                        $.getScript("/js/dragscroll.js", function () {
                        });
                    }
                });
                $('.ajax-loading-model').fadeOut();
            });	//end ajax call			


        });
       
    </script>--%>
</asp:Content>

