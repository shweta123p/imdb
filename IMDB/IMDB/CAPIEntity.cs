﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMDB
{
    public class CAPIEntity
    {
        public const string base_url = "http://devapi.gstsystem.co.in";
        public const string URL_Authentication = "/taxpayerapi/v0.2/authenticate";
        public const string REG_DSC = "/taxpayerapi/v0.2/registerdsc";
        public const string Get_Ledger = "/taxpayerapi/v0.3/ledgers";
        public const string Get_File = base_url + "/taxpayerapi-doc";
        public const string URL_GSTR = "/taxpayerapi/v0.3/returns/gstr1";
        //public const string URL_GSTR = "/taxpayerapi/v0.3/returns/gstr2";

        public const string ClientId = "l7xx6f23555f01164844a3b65bf5eaaf7dad";     // Your Sandbox ClientId
        public const string ClientSecret = "a95b757893b04aadbb70df1f490acce3"; // Your Sandbox ClientSecret

        public static byte[] AppKeyBytes;
        public const string StateCode = "27";
        public const string txn = "LAPN242353255";
        public const string UserIP = "203.77.201.131";
        public const string Ret_Period = "102017";

        public const string strActivityId="";
        
        public const string UserName = "GujaratLivelihood.MH.TP.1"; // Your Sandbox User ID
        public const string GSTIN = "27GSP281G2Z6"; // Your Sandbox GSTIN Number
        public const string CTIN = "27GSPMH9281G2Z6"; // Your Sandbox GSTIN Number  
    }
}