﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GSP
{
    public partial class _Default : Page
    {
        DataView dv_links = new DataView();

        BusinessLogic.Movie objMovie = new BusinessLogic.Movie();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet dsMovies = new DataSet();
                dsMovies = objMovie.GetMovieDetails();
                if (dsMovies != null)
                {
                    if (dsMovies.Tables[0].Rows.Count > 0)
                    {
                        repMovieList.DataSource = dsMovies.Tables[0];
                        repMovieList.DataBind();
                    }

                }
            }
        }

        protected void repMovieList_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string movieId = "";
                    movieId = e.CommandArgument.ToString();

                    if (!string.IsNullOrEmpty(movieId))
                    {
                        Response.Redirect("/Movies.aspx?movieId=" + movieId, false);
                    }
                }
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception", true);
                BusinessLogic.Error.WriteToLogFile("Error On repMovieList_OnItemCommand in Default.aspx: " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        protected void btnAddMovie_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/Movies.aspx", false);
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception", true);
                BusinessLogic.Error.WriteToLogFile("Error On btnAddMovie_Click in Default.aspx  : " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

    }
}