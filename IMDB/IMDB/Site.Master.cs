﻿using BusinessLogic;
using System;
using System.Configuration;
using System.Web.UI;


namespace GSP
{
    public partial class SiteMaster : MasterPage
    {
        Actor objActor = new Actor();
        Producer objProducer = new Producer();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnAddActor_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    if (btnAddActor.Text == "Add")
                    {
                        Boolean status = objActor.InsertActor(txtActorname.Text.Trim(), rblforgender.SelectedValue, txtdob.Text.Trim(), txtbio.Text.Trim());

                        if (status == true)
                        {
                            ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "CallMyFunction", "$('#exampleModal').modal('toggle'); swal({   title: 'Success',   text: 'Actor Details Added Successfully..',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false },function(){ window.location.reload();});", true);
                            ClearActor();
                        }
                    }
                    else if (btnAddActor.Text == "Update")
                    {
                        Boolean status = objActor.UpdateActor(hfActorId.Value, txtActorname.Text.Trim(), rblforgender.SelectedValue, txtdob.Text.Trim(), txtbio.Text.Trim());

                        if (status == true)
                        {
                            ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "CallMyFunction", "$('#exampleModal').modal('toggle'); swal({   title: 'Success',   text: 'Actor Details Updated Successfully..',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false },function(){window.location='/Movie.aspx';});", true);
                            ClearActor();
                            btnAddActor.Text = "Add";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception");
                BusinessLogic.Error.WriteToLogFile("Error On btnAddActor_Click in SiteMaster.aspx: " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearActor();
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception");
                BusinessLogic.Error.WriteToLogFile("Error On btncancel_Click in SiteMaster.aspx: " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        protected void btnaddProducer_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    if (btnAddActor.Text == "Add")
                    {
                        Boolean status = objProducer.InsertProducer(txtproducer.Text.Trim(), rblpgender.SelectedValue, txtpbdate.Text.Trim(), txtpbio.Text.Trim());

                        if (status == true)
                        {
                            ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "CallMyFunction", "$('#addnewgstin').modal('toggle'); swal({   title: 'Success',   text: 'Producer Details Added Successfully..',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false },function(){ window.location.reload();});", true);
                            ClearProducer();
                        }
                    }
                    else if (btnAddActor.Text == "Update")
                    {
                        Boolean status = objProducer.UpdateProducer(hfProducerId.Value, txtproducer.Text.Trim(), rblpgender.SelectedValue, txtpbdate.Text.Trim(), txtpbio.Text.Trim());

                        if (status == true)
                        {
                            ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "CallMyFunction", "$('#addnewgstin').modal('toggle'); swal({   title: 'Success',   text: 'Producer Details Updated Successfully..',  type: 'success',showCancelButton: false,confirmButtonClass: 'btn-info',confirmButtonText: 'Ok',closeOnConfirm: false },function(){window.location='/Movie.aspx';});", true);
                            ClearProducer();
                            btnaddProducer.Text = "Add";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception");
                BusinessLogic.Error.WriteToLogFile("Error On btnaddProducer_Click in SiteMaster.aspx: " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        protected void btnCancelProducer_Click(object sender, EventArgs e)
        {
            try
            {
                ClearProducer();
            }
            catch (Exception ex)
            {
                BusinessLogic.msg.GetMsg(this.Page, ex.Message, "Exception");
                BusinessLogic.Error.WriteToLogFile("Error On btnCancelProducer_Click in SiteMaster.aspx: " + ex.Message, ConfigurationManager.AppSettings["LogPath"].ToString());
            }
        }

        public void ClearProducer()
        {
            txtproducer.Text = string.Empty;
            txtpbdate.Text = string.Empty;
            txtpbio.Text = string.Empty;
            rblpgender.SelectedValue = "1";

            btnaddProducer.Text = "Add";
        }

        public void ClearActor()
        {
            txtActorname.Text = string.Empty;
            txtdob.Text = string.Empty;
            txtbio.Text = string.Empty;
            rblforgender.SelectedValue = "1";

            btnAddActor.Text = "Add";
        }

    }
}