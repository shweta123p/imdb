﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace IMDB
{
    public class BundleConfig
    {
        // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/js/WebFormsJs").Include(
                            "~/Scripts/WebForms/WebForms.js",
                            "~/Scripts/WebForms/WebUIValidation.js",
                            "~/Scripts/WebForms/MenuStandards.js",
                            "~/Scripts/WebForms/Focus.js",
                            "~/Scripts/WebForms/GridView.js",
                            "~/Scripts/WebForms/DetailsView.js",
                            "~/Scripts/WebForms/TreeView.js",
                            "~/Scripts/WebForms/WebParts.js"));

            bundles.Add(new ScriptBundle("~/js").Include(
                                        "~/js/jqueryCombine.min.js",                                        
                                        "~/js/bootstrap.min.js",
                                        "~/js/jquery.slimscroll.js",
                                        "~/js/waves.js",                                       
                                        "~/js/jquery.counterup.min.js",
                                        "~/js/raphael-min.js",
                                        "~/js/morris.js",
                                        "~/js/jquery.toast.js",
                                        "~/js/sweetalert.min.js" ,
                                        "~/js/jquery.sweet-alert.custom.js",
                                        "~/js/isotope.pkgd.min.js",
                                        "~/js/imagesloaded.pkgd.min.js",
                                        "~/js/masonry.pkgd.min.js",
                                        "~/js/select2.min.js",
                                        //"~/js/custom-select.min.js",
                                        "~/js/custom.min.js",
                                        "~/js/dropify.min.js",
                                        "~/js/jquery.dataTables.min.js",
                                        "~/js/dataTables.bootstrap.min.js"
                                      ));


     

            // Order is very important for these files to work, they have explicit dependencies
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            // Use the Development version of Modernizr to develop with and learn from. Then, when you’re
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-*"));

            ScriptManager.ScriptResourceMapping.AddDefinition(
                "respond",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/respond.min.js",
                    DebugPath = "~/Scripts/respond.js",
                });
        }
    }
}