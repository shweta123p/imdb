﻿function setActivityLog(name, data) {
    removeActivityLog(name);
    window.localStorage.setItem(name, data);
    return;
}

function getActivityLog(name) {
    return window.localStorage.getItem(name);
}

function appendActivityLog() {
    var name = 'log';
    var activityLog = getActivityLog(name);

    var date = new Date();
    var formatedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

    var data = {
        url: document.location.href,
        time: formatedDate
    }

    data = JSON.stringify(data);

    if (activityLog != null && activityLog != "") {
        activityLog = activityLog + ' -> ' + data;
        setActivityLog(name, activityLog);
    } else {
        setActivityLog(name, data);
    }
}

function removeActivityLog(name) {
    window.localStorage.removeItem(name);
    return;
}