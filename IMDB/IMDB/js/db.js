(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.Name )
                    && (!filter.Age )
                    && (!filter.Address )
                    && (!filter.Country )
                    && (filter.Married === undefined );
            });
        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
        },

        updateItem: function(updatingClient) { },

        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1);
        }

    };

    window.db = db;


    db.countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 },
        { Name: "France", Id: 4 },
        { Name: "Brazil", Id: 5 },
        { Name: "China", Id: 6 },
        { Name: "Russia", Id: 7 }
    ];

    db.clients = [
        {
            "Name": "Otto Clay",
            "Age": 61,
            "Country": 6,
            "Address": 23,
            "Married": 21,
			"Married2": 11
        },
        {
            "Name": "Otto Clay",
            "Age": 61,
            "Country": 6,
            "Address": 23,
            "Married": 21,
			"Married2": 11
        },
        {
            "Name": "Otto Clay",
            "Age": 61,
            "Country": 6,
            "Address": 23,
            "Married": 21,
			"Married2": 11
        },
        {
            "Name": "Otto Clay",
            "Age": 61,
            "Country": 6,
            "Address": 23,
            "Married": 21,
			"Married2": 11
        }
    ];


}());