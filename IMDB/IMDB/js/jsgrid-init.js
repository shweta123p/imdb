! function(document, window, $) {
    "use strict";
    var Site = window.Site;
    $(document).ready(function($) {
            
        }), jsGrid.setDefaults({
            tableClass: "jsgrid-table table table-striped table-hover"
        }),
        function() {
            $("#basicgrid").jsGrid({
                height: "500px",
                width: "100%",
                filtering: false,
				selecting: false,
                editing: true,				
				heading:true,
                sorting: false,
                paging: false,
                autoload: true,
                pageSize: 15,
                pageButtonCount: 5,               
                controller: db,
                fields: [{
                    name: "Nature Of Supplies",
                    type: "text",
                    width: 200
                }, {
                    name: "Total Taxable Value",
                    type: "number",
                    width: 70
                }, {
                    name: "Integrated Tax",
                    type: "number",
                    width: 70
                }, {
                    name: "Central Tax	",
                    type: "number",
                    width: 70
                }, {
                    name: "State/UT Tax",
                    type: "number",
                    width: 70
                }, {
                    name: "cess",
                    type: "number",
                    width: 70
                },{
                    type: "control", deleteButton: false
                }]
            })
        }(),
                
        function() {
            var MyDateField = function(config) {
                jsGrid.Field.call(this, config)
            };
            MyDateField.prototype = new jsGrid.Field({
                sorter: function(date1, date2) {
                    return new Date(date1) - new Date(date2)
                },
                itemTemplate: function(value) {
                    return new Date(value).toDateString()
                },
                insertTemplate: function() {
                    if (!this.inserting) return "";
                    var $result = this.insertControl = this._createTextBox();
                    return $result
                },
                editTemplate: function(value) {
                    if (!this.editing) return this.itemTemplate(value);
                    var $result = this.editControl = this._createTextBox();
                    return $result.val(value), $result
                },
                insertValue: function() {
                    return this.insertControl.datepicker("getDate")
                },
                editValue: function() {
                    return this.editControl.datepicker("getDate")
                },
                _createTextBox: function() {
                    return $("<input>").attr("type", "text").addClass("form-control input-sm").datepicker({
                        autoclose: true
                    })
                }
            })
        }()
}(document, window, jQuery);