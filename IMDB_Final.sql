USE [IMDB]
GO
/****** Object:  Table [dbo].[ActorMovieMapping]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActorMovieMapping](
	[actorMovieMappingId] [int] IDENTITY(1,1) NOT NULL,
	[actorId] [int] NULL,
	[movieId] [int] NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[modifiedBy] [int] NULL,
	[modifiedDate] [datetime] NULL,
	[isDeleted] [bit] NULL CONSTRAINT [DF_ActorMovieMapping_isDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_ActorMovieMapping] PRIMARY KEY CLUSTERED 
(
	[actorMovieMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Actors]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actors](
	[actorId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
	[gender] [nvarchar](10) NULL,
	[dateOfBirth] [datetime] NULL,
	[bio] [nvarchar](max) NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[modifiedBy] [int] NULL,
	[modifiedDate] [datetime] NULL,
	[isDeleted] [bit] NULL CONSTRAINT [DF_Actors_isDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[actorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovieProducerMapping]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieProducerMapping](
	[movieProducerMappingId] [int] IDENTITY(1,1) NOT NULL,
	[producerId] [int] NULL,
	[movieId] [int] NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[modifiedBy] [int] NULL,
	[modifiedDate] [datetime] NULL,
	[isDeleted] [bit] NULL CONSTRAINT [DF_MovieProducerMapping_isDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_MovieProducerMapping] PRIMARY KEY CLUSTERED 
(
	[movieProducerMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Movies]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movies](
	[movieId] [int] IDENTITY(1,1) NOT NULL,
	[movieName] [nvarchar](100) NULL,
	[yearOfRelease] [nvarchar](50) NULL,
	[ploat] [nvarchar](max) NULL,
	[posterPath] [nvarchar](100) NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[modifiedBy] [int] NULL,
	[modifiedDate] [datetime] NULL,
	[isDeleted] [bit] NULL CONSTRAINT [DF_Movies_isDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Movies] PRIMARY KEY CLUSTERED 
(
	[movieId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Producers]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producers](
	[producerId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
	[gender] [nvarchar](10) NULL,
	[dateOfBirth] [datetime] NULL,
	[bio] [nvarchar](max) NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[modifiedBy] [int] NULL,
	[modifiedDate] [datetime] NULL,
	[isDeleted] [bit] NULL CONSTRAINT [DF_Producers_isDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Producers] PRIMARY KEY CLUSTERED 
(
	[producerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserMaster]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMaster](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NOT NULL,
	[Password] [varbinary](max) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[isDeleted] [bit] NOT NULL,
	[IpAddress] [nvarchar](500) NULL,
 CONSTRAINT [PK_UserMaster] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UserMaster] ADD  CONSTRAINT [DF_UserMaster_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
/****** Object:  StoredProcedure [dbo].[ActorList]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActorList]

AS
BEGIN

	SELECT actorId,name, CASE WHEN gender = 1 THEN 'Male' WHEN gender = 2 THEN 'Female' WHEN gender = 3 THEN 'Other' END AS gender,
	CONVERT(NVARCHAR(20),dateOfBirth,103) AS dateOfBirth,bio
	FROM dbo.Actors WHERE isDeleted = 0
END

GO
/****** Object:  StoredProcedure [dbo].[ActorListById]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActorListById]
@actorId INT
AS
BEGIN

	SELECT actorId,name, CASE WHEN gender = 1 THEN 'Male' WHEN gender = 2 THEN 'Female' WHEN gender = 3 THEN 'Other' END AS gender,
	CONVERT(NVARCHAR(20),dateOfBirth,103) AS dateOfBirth,bio
	FROM dbo.Actors 
	WHERE isDeleted = 0 AND actorId = @actorId
END

GO
/****** Object:  StoredProcedure [dbo].[FillActor]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FillActor]

AS
BEGIN

	SELECT actorId AS 'value', name AS 'text'
	FROM dbo.Actors WHERE isDeleted = 0
END

GO
/****** Object:  StoredProcedure [dbo].[FillProducer]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FillProducer]

AS
BEGIN

	SELECT producerId AS 'value', name AS 'text'
	FROM dbo.Producers WHERE isDeleted = 0
END

GO
/****** Object:  StoredProcedure [dbo].[GetMovieDetails]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17-08-2018
-- =============================================
CREATE PROCEDURE [dbo].[GetMovieDetails]

AS
BEGIN
	SET NOCOUNT OFF;

	SELECT m.movieId,m.movieName,m.ploat,p.producerId,p.name AS 'Producer',m.yearOfRelease,m.posterPath
	FROM dbo.Movies AS m INNER JOIN dbo.MovieProducerMapping mpm ON m.movieId = mpm.movieId AND mpm.isDeleted = 0
	INNER JOIN dbo.Producers p ON p.producerId = mpm.producerId AND p.isDeleted = 0 
	WHERE m.isDeleted = 0

	SELECT m.movieId,a.actorId,a.name AS 'Actor',
	CASE WHEN a.gender = 1 THEN 'Male' WHEN a.gender = 2 THEN 'Female' WHEN a.gender = 3 THEN 'Other' END AS gender,
	CONVERT(NVARCHAR(20),a.dateOfBirth,103) AS dateOfBirth,a.bio
	FROM dbo.Movies AS m 
	INNER JOIN dbo.ActorMovieMapping amm ON amm.movieId = m.movieId AND amm.isDeleted = 0 
	INNER JOIN dbo.Actors a ON a.actorId = amm.actorId AND a.isDeleted = 0
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetMovieDetailsbyId]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17-08-2018
-- =============================================
CREATE PROCEDURE [dbo].[GetMovieDetailsbyId] --1
@MovieId INT
AS
BEGIN
	SET NOCOUNT OFF;

	SELECT m.movieId,m.movieName,m.ploat,p.producerId,p.name AS 'Producer',m.yearOfRelease,m.posterPath
	FROM dbo.Movies AS m INNER JOIN dbo.MovieProducerMapping mpm ON m.movieId = mpm.movieId AND mpm.isDeleted = 0
	INNER JOIN dbo.Producers p ON p.producerId = mpm.producerId AND p.isDeleted = 0 
	WHERE m.movieId = @MovieId AND m.isDeleted = 0

	SELECT m.movieId,a.actorId,a.name,
	CASE WHEN a.gender = 1 THEN 'Male' WHEN a.gender = 2 THEN 'Female' WHEN a.gender = 3 THEN 'Other' END AS gender,
	CONVERT(NVARCHAR(20),a.dateOfBirth,103) AS dateOfBirth,a.bio
	FROM dbo.Movies AS m 
	INNER JOIN dbo.ActorMovieMapping amm ON amm.movieId = m.movieId AND amm.isDeleted = 0 
	INNER JOIN dbo.Actors a ON a.actorId = amm.actorId AND a.isDeleted = 0
	WHERE m.movieId = @MovieId AND m.isDeleted = 0

END



GO
/****** Object:  StoredProcedure [dbo].[InsertActor]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertActor]
@name NVARCHAR(200),
@gender NVARCHAR(10),
@dateofBirth NVARCHAR(20),
@bio NVARCHAR(MAX)
AS
BEGIN

IF NOT EXISTS(SELECT * FROM dbo.Actors WHERE name = LTRIM(RTRIM(@name))  AND isDeleted = 0)
BEGIN
	INSERT INTO [dbo].[Actors]
           ([name],[gender],[dateOfBirth],[bio],[createdBy],[createdDate],[isDeleted])
     VALUES
           (@name,@gender,CONVERT(DATE,@dateofBirth,103),@bio,1,GETDATE(),0)
END
END

GO
/****** Object:  StoredProcedure [dbo].[InsertMovieDetail]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertMovieDetail]
@movieName NVARCHAR(200),
@yearOfRelease NVARCHAR(50),
@ploat NVARCHAR(MAX),
@posterPath NVARCHAR(MAX),
@producerId INT,
@actorId NVARCHAR(MAX)
AS
BEGIN
 BEGIN TRY
        BEGIN TRAN;

	IF NOT EXISTS(SELECT * FROM dbo.Movies WHERE movieName = LTRIM(RTRIM(@movieName)) AND isDeleted = 0)
	BEGIN
		INSERT INTO [dbo].[Movies]
           ([movieName],[yearOfRelease],[ploat],[posterPath]
           ,[createdBy],[createdDate],[isDeleted])
		VALUES
           (@movieName,@yearOfRelease,@ploat,@posterPath,
		   1,GETDATE(),0)
		
		DECLARE @movieId INT
		SET @movieId = SCOPE_IDENTITY();

		INSERT INTO [dbo].[MovieProducerMapping]
           ([producerId],[movieId],[createdBy],[createdDate],[isDeleted])
		VALUES
           (@producerId,@movieId,1,GETDATE(),0)
		
		INSERT INTO [dbo].[ActorMovieMapping]
           ([actorId],[movieId],[createdBy],[createdDate],[isDeleted])
		   SELECT OrderID,@movieId,1,GETDATE(),0 FROM dbo.SplitIDs(@actorId)
	END
 COMMIT TRAN;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN;
            RAISERROR('Movie insert failed', 1, 1);
            RETURN;
        END;
    END CATCH;
END;

GO
/****** Object:  StoredProcedure [dbo].[InsertProducer]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertProducer]
@name NVARCHAR(200),
@gender NVARCHAR(10),
@dateofBirth NVARCHAR(20),
@bio NVARCHAR(MAX)
AS
BEGIN

	IF NOT EXISTS(SELECT * FROM dbo.Producers WHERE name = LTRIM(RTRIM(@name)) AND isDeleted = 0)
	BEGIN
		INSERT INTO [dbo].Producers
		(
			name,gender,dateOfBirth,bio,createdBy,createdDate,isDeleted
		)
		VALUES
		(  @name,@gender,CONVERT(DATE,@dateofBirth,103),@bio,1,GETDATE(),0)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[InsertUserDetails]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 16/08/2018
-- =============================================
CREATE PROCEDURE [dbo].[InsertUserDetails]
 @Name NVARCHAR(50),
    @MobileNo NUMERIC(18, 0),
    @UserName NVARCHAR(25),
    @EmailId NVARCHAR(50),
    @policyId INT,
    @createdBy INT,
    @userTypeId INT
AS
BEGIN
	  OPEN SYMMETRIC KEY PasswordFieldSymmetricKey
    DECRYPTION BY CERTIFICATE PasswordFieldCertificate;

    -- SET NOCOUNT ON added to prevent extra res	ult sets from
    -- interfering with SELECT statements.
    SET NOCOUNT OFF;

  
        BEGIN TRY
            BEGIN TRANSACTION;

            DECLARE @pwd AS VARCHAR(100);
            SET @pwd = '123456';

            INSERT INTO dbo.UserMaster
            (
                UserName,
                [Password],
                CreatedBy,
                CreatedDate
            )
            VALUES
            (@UserName, ENCRYPTBYKEY(KEY_GUID('PasswordFieldSymmetricKey'), @pwd), 1,dbo.currDate());

      
            COMMIT TRANSACTION;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;

            DECLARE @ERROR_MESSAGE VARCHAR(MAX),
                    @ERROR_SEVERITY VARCHAR(MAX),
                    @ERROR_STATE VARCHAR(MAX),
                    @ERROR_NUMBER VARCHAR(MAX);

            SET @ERROR_SEVERITY = ERROR_SEVERITY();
            SET @ERROR_STATE = ERROR_STATE();
            SET @ERROR_NUMBER = ERROR_NUMBER();
            SET @ERROR_MESSAGE = ERROR_MESSAGE();

            --On error RaiseError  
            RAISERROR(@ERROR_MESSAGE, @ERROR_SEVERITY, @ERROR_STATE, @ERROR_NUMBER);

        --Catch block Ends here
        END CATCH;
    END;
   

GO
/****** Object:  StoredProcedure [dbo].[ProducerList]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ProducerList]

AS
BEGIN

	SELECT producerId,name, CASE WHEN gender = 1 THEN 'Male' WHEN gender = 2 THEN 'Female' WHEN gender = 3 THEN 'Other' END AS gender,
	CONVERT(NVARCHAR(20),dateOfBirth,103) AS dateOfBirth,bio
	FROM dbo.Producers WHERE isDeleted = 0
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateActor]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateActor]
@actorId INT,
@name NVARCHAR(200),
@gender NVARCHAR(10),
@dateofBirth NVARCHAR(20),
@bio NVARCHAR(MAX)
AS
BEGIN

	IF NOT EXISTS
    (
        SELECT actorId FROM dbo.Actors WHERE actorId <> @actorId AND name = @name AND isDeleted = 0
    )
    BEGIN
		UPDATE [dbo].[Actors]
		SET [name] = @name,[gender] = @gender,[dateOfBirth] = CONVERT(DATE,@dateofBirth,103)
		,[bio] = @bio,[modifiedBy] = 1,[modifiedDate] = GETDATE()
		WHERE actorId = @actorId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateMovieDetail]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateMovieDetail]
@movieId INT,
@movieName NVARCHAR(200),
@yearOfRelease NVARCHAR(50),
@ploat NVARCHAR(MAX),
@producerId INT,
@actorId NVARCHAR(MAX)
AS
BEGIN
 BEGIN TRY
        BEGIN TRAN;

	IF NOT EXISTS(SELECT movieId FROM dbo.Movies WHERE movieId <> @movieId AND movieName = LTRIM(RTRIM(@movieName)) AND isDeleted = 0)
	BEGIN
		UPDATE [dbo].[Movies]
		   SET [movieName] = @movieName
			  ,[yearOfRelease] = @yearOfRelease
			  ,[ploat] = @ploat
			  --,[posterPath] = @posterPath
			  ,[modifiedBy] = 1
			  ,[modifiedDate] = GETDATE()
		 WHERE movieId = @movieId

		UPDATE [dbo].[MovieProducerMapping]
		   SET [producerId] = @producerId
			  ,[modifiedBy] = 1
			  ,[modifiedDate] = GETDATE()
		 WHERE movieId = @movieId

		 SELECT * INTO #tempActors FROM dbo.SplitIDs(@actorId)

		 UPDATE [dbo].[ActorMovieMapping]
		 SET [isDeleted] = 1
		WHERE movieId = @movieId AND actorId NOT IN (SELECT OrderID FROM #tempActors)

	
		 INSERT INTO [dbo].[ActorMovieMapping]
			   ([actorId],[movieId],[createdBy],[createdDate],[isDeleted])
		SELECT OrderID,@movieId,1,GETDATE(),0 FROM #tempActors WHERE OrderID NOT IN (SELECT actorId FROM dbo.ActorMovieMapping WHERE movieId = @movieId) 

	END
 COMMIT TRAN;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN;
            RAISERROR('Movie insert failed', 1, 1);
            RETURN;
        END;
    END CATCH;
END;

GO
/****** Object:  StoredProcedure [dbo].[UpdateProducer]    Script Date: 8/24/2018 9:10:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shweta Pipaliya
-- Create date: 17/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateProducer]
@producerId INT,
@name NVARCHAR(200),
@gender NVARCHAR(10),
@dateofBirth NVARCHAR(20),
@bio NVARCHAR(MAX)
AS
BEGIN

	IF NOT EXISTS
    (
        SELECT producerId FROM dbo.Producers WHERE producerId <> @producerId AND name = @name AND isDeleted = 0
    )
    BEGIN
		UPDATE [dbo].[Producers]
		SET [name] = @name,[gender] = @gender,[dateOfBirth] = CONVERT(DATE,@dateofBirth,103)
		,[bio] = @bio,[modifiedBy] = 1,[modifiedDate] = GETDATE()
		WHERE producerId = @producerId
	END
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-male,2-female,3-other' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Actors', @level2type=N'COLUMN',@level2name=N'gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-male,2-female,3-other' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producers', @level2type=N'COLUMN',@level2name=N'gender'
GO
