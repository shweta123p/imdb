
You have to do perform following steps for setup and run web application:

- Accept Invitation from Bitbucket where you will Find Source Code.
- .NET Framework 4.5 is used for Web Application. 
- All the DLLs and references add in the Solution.
- To Run the Solution you have to first Import Database.
- Script is Provided with Source Code on Bitbucket.(File Name:IMDB_Final)
- Create Database and execute the Script. 
- Provide Database Credential in Web.Config file in [ConnectionString,IMDBConnectionString] Parameters in Solution.
- Run the Solution.
- redgate products to inhibit online checks.

Notes: 
- Visual Studio 2017 is Used for the Web Application.
- Sql Server 2012 is used for Database.

